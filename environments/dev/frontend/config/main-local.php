<?php

$config = [
    'components' => [
        'view' => [
            'theme' => [
                'basePath' => '@webroot/themes/sport',
                'baseUrl'  => '@web/themes/sport',
                'pathMap'  => [
                    '@frontend/views'   => '@webroot/themes/sport',
                    '@frontend/widgets' => '@webroot/themes/sport/widgets',
                ],
            ],
        ],

        'authClientCollection' => [
            'class'   => 'yii\authclient\Collection',
            'clients' => [
                'yandex'   => [
                    'class'        => 'yii\authclient\clients\Yandex',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'google'   => [
                    'class'        => 'yii\authclient\clients\Google',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'facebook' => [
                    'class'        => 'yii\authclient\clients\Facebook',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'vk'       => [
                    'class'        => 'yii\authclient\clients\VKontakte',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
            ],
        ]
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][]    = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['*'],
    ];
}

return $config;
