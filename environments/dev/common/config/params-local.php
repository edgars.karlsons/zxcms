<?php
return [
    'cookieValidationKey' => '',
    'cookieDomain'        => '',
    'staticPath'          => dirname(__DIR__, 2) . '/static',
    'frontendHostInfo'    => '',
    'backendHostInfo'     => '',
    'staticHostInfo'      => '',
    'supportEmail'        => '',
    'adminEmail'          => '',
    'mailChimpKey'        => '',
    'mailChimpListId'     => '',
];

/*
    'cookieDomain'        => '.zxcms.local',
    'staticPath'          => dirname(__DIR__, 2) . '/static',
    'frontendHostInfo'    => 'http://zxcms.local',
    'backendHostInfo'     => 'http://admin.zxcms.local',
    'staticHostInfo'      => 'http://static.zxcms.local',
 */
