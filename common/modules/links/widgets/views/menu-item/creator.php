<?php

use core\forms\menu\MenuItemForm;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this View
 * @var $model MenuItemForm
 */

?>

<div class="menu_item_widget">

<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')->input('text', [
        'class' => 'form-control form-control-sm'
    ]) ?>

	<?= $form->field($model, 'title_attr')->input('text', [
        'class' => 'form-control form-control-sm'
    ]) ?>

	<?= $form->field($model, 'module')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'menu_id')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'url')->input('text', [
        'class' => 'form-control form-control-sm'
    ]) ?>

	<?= $form->field($model, 'url_params')->hiddenInput()->label(false) ?>

<div class="form-group">
<?= Html::submitButton(Yii::t('buttons', 'Add to menu'), [
	'class' => 'btn btn-info btn-sm float-right'
]) ?>
</div>

<?php ActiveForm::end(); ?>

</div>