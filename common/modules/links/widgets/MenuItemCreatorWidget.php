<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

namespace common\modules\links\widgets;

use core\forms\menu\MenuItemForm;
use yii\base\Widget;

class MenuItemCreatorWidget extends Widget
{
	public int $menu_id;

	public function run(): string
    {
		$form = new MenuItemForm();
		$form->module = 'links';
		$form->menu_id = $this->menu_id;

		return $this->render('menu-item/creator', [
			'model' => $form,
		]);
	}
}