<?php

namespace common\modules\links;

use common\modules\links\widgets\MenuItemCreatorWidget;
use core\components\modules\ModuleInterface;
use Yii;
use yii\base\Module;
use yii\helpers\ArrayHelper;
use yii\web\Application;

/**
 * blog module definition class
 */
class LinksModule extends Module implements ModuleInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\links\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

	public function bootstrap(Application $app)
	{
		// add languages
		$app->getI18n()->translations = ArrayHelper::merge($app->getI18n()->translations, [
			'links' => [
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@common/modules/links/messages',
			],
			'link_public' => [
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@common/modules/links/messages',
			],
		]);
	}

	public static function getMenuItemCreator($menu_id): array
	{
		$widgets = [];
		$widgets[] = [
			'id' => 'links',
			'title' => Yii::t('links', 'Links'),
			'content' => MenuItemCreatorWidget::widget([
				'menu_id' => $menu_id,
			]),
		];
		return $widgets;
	}
}
