<?php

return [
    'version'     => '1.0.1',
    'name'        => 'forms',
    'description' => 'Form widget generator for site',
    'module'      => 'FormsModule',
    'permissions' => [
        'FormsManagement' => 'Forms management',
    ],
];