<?php
/**
 * Created by Error202
 * Date: 31.07.2018
 */

namespace common\modules\forms\widgets;

use common\modules\forms\entities\Form;
use yii\base\Widget;

class FormWidget extends Widget
{
	public int $id;

	public function run(): string
    {
		$form = Form::findOne($this->id);
		if (!$form) {
			return 'Form is not found';
		}
		$className = 'DynaForm' . $this->id;
		$classPath = '\\common\\modules\\forms\\runtime\\' . $className;
		$dynaForm = new $classPath();

		return $this->renderFile(\Yii::getAlias('@common/modules/forms/runtime/' . 'DynaView' . $this->id . '.php'), ['model' => $dynaForm, 'form_entity' => $form]);
	}
}
