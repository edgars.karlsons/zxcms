<?php
/**
 * Created by Error202
 * Date: 02.08.2018
 */

namespace common\modules\forms\widgets;

class FormViewField
{
	public function text(array $settings): string
	{
		$options = [];
		$options[] = isset($item['placeholder']) ? "'placeholder' => '{$settings['placeholder']}'" : "";
		$options[] = isset($item['value']) ? "'value' => '{$settings['value']}'" : "";
		$options[] = isset($item['maxlength']) ? "'maxlength' => true" : "";
		$options = implode(',', array_filter($options));
		$description = isset($item['description']) ? $settings['description'] : '';
		$field = "<?= \$form->field(\$model, '{$settings['name']}')->textInput([{$options}])->hint('{$description}') ?>";
		return $field;
	}

	public function textArea(array $settings): string
	{
		$options = [];
		$options[] = isset($item['rows']) ? "'rows' => {$settings['rows']}" : "";
		$options[] = isset($item['placeholder']) ? "'placeholder' => '{$settings['placeholder']}'" : "";
		$options[] = isset($item['value']) ? "'value' => '{$settings['value']}'" : "";
		$options = implode(',', array_filter($options));
		$description = isset($item['description']) ? $settings['description'] : '';
		$field = "<?= \$form->field(\$model, '{$settings['name']}')->textarea([{$options}])->hint('{$description}') ?>";
		return $field;
	}

	public function header(array $settings): string
	{
        return "<{$settings['subtype']}>{$settings['label']}</{$settings['subtype']}>";
	}

	public function paragraph(array $settings): string
	{
        return "<{$settings['subtype']}>{$settings['label']}</{$settings['subtype']}>";
	}

	public function hidden(array $settings): string
	{
		$options = [];
		$options[] = isset($item['value']) ? "'value' => '{$settings['value']}'" : "";
		$options = implode(',', array_filter($options));
        return "<?= \$form->field(\$model, '{$settings['name']}')->hiddenInput([{$options}])->label(false) ?>";
	}

	public function radioGroup(array $settings): string
	{
		$values = [];
		$selected = [];
		foreach ($settings['values'] as $value) {
			$values[] = "'{$value['value']}' => '{$value['label']}'";
			if (isset($value['selected']) && $value['selected'] == true) {
				$selected[] = "'{$value['value']}'";
			}
		}
		$values = implode(',', $values);
		$selected = implode(',', $selected);

		if ($selected) {
			$fields[] = "<?php \$model->{$settings['name']} = [{$selected}] ?>";
		}

		$options = [];
		$options = implode(',', array_filter($options));
		$description = $settings['description'] ?? '';
		return "<?= \$form->field(\$model, '{$settings['name']}')
                    ->radioList([
				        {$values}
                ], [{$options}])->hint('{$description}'); ?>";
	}

	public function checkboxGroup(array $settings): string
	{
		$values = [];
		$selected = [];
		foreach ($settings['values'] as $value) {
			$values[] = "'{$value['value']}' => '{$value['label']}'";
			if (isset($value['selected']) && $value['selected'] == true) {
				$selected[] = "'{$value['value']}'";
			}
		}
		$values = implode(',', $values);
		$selected = implode(',', $selected);

		if ($selected) {
			$fields[] = "<?php \$model->{$settings['name']} = [{$selected}] ?>";
		}

		$options = [];
		$options = implode(',', array_filter($options));
		$description = $settings['description'] ?? '';
		return "<?= \$form->field(\$model, '{$settings['name']}')
                    ->checkboxList([
				        {$values}
                ], [{$options}])->hint('{$description}'); ?>";
	}

	public function select(array $settings): string
	{
		$values = [];
		$selected = [];
		foreach ($settings['values'] as $value) {
			$values[] = "'{$value['value']}' => '{$value['label']}'";
			if (isset($value['selected']) && $value['selected'] == true) {
				$selected[] = "'{$value['value']}'";
			}
		}
		$values = implode(',', $values);
		$selected = implode(',', $selected);

		$options = [];
		$options[] = $selected ? "'value' => [{$selected}]" : "";
		$options[] = isset($settings['placeholder']) ? "'prompt' => '{$settings['placeholder']}'" : "";
		$options[] = isset($settings['multiple']) && $settings['multiple'] == true ? "'multiple' => 'multiple'" : "";
		$options = implode(',', array_filter($options));
		$description = $settings['description'] ?? '';
		return "<?= \$form->field(\$model, '{$settings['name']}')->dropDownList([{$values}], [{$options}])->hint('{$description}') ?>";
	}

	public function button(array $settings): string
	{
		$style = $settings['style'] ?? 'default';
		if ($settings['subtype'] == 'submit') {
            return "<div class=\"form-group\"><?= Html::submitButton('{$settings['label']}', ['class' => 'btn btn-{$style}']) ?></div>";
		}
		elseif ($settings['subtype'] == 'button') {
            return "<div class=\"form-group\"><?= Html::button('{$settings['label']}', ['class' => 'btn btn-{$style}']) ?></div>";
		}
		elseif ($settings['subtype'] == 'reset') {
            return "<div class=\"form-group\"><?= Html::resetButton('{$settings['label']}', ['class' => 'btn btn-{$style}']) ?></div>";
		}
		return '';
	}
}
