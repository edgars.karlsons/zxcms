<?php

namespace common\modules\forms\services;

use common\modules\forms\entities\Form;
use common\modules\forms\forms\FormForm;
use common\modules\forms\repositories\FormRepository;
use common\modules\forms\widgets\FormGenerator;
use yii\db\StaleObjectException;
use yii\helpers\Json;

class FormManageService
{
    private FormRepository $repository;

    public function __construct(FormRepository $repository)
    {
        $this->repository = $repository;
    }

    public function clearJsonLabels($json): string
    {
    	$new_data = [];
    	$data = Json::decode($json, true);
    	foreach ($data as $item) {
    		if (isset($item['label'])) {
			    $item['label'] = str_replace( '<br>', '', $item['label'] );
		    }
    		$new_data[] = $item;
	    }
	    return Json::encode($new_data, JSON_UNESCAPED_UNICODE);
    }

    public function create(FormForm $form): Form
    {
        $form = Form::create(
            $form->name,
            $this->clearJsonLabels($form->data),
	        $form->subject,
            $form->from,
            $form->reply,
            $form->return,
            $form->complete_text,
            $form->complete_page_id,
            $form->status,
            $form->captcha
        );
        $this->repository->save($form);
	    FormGenerator::generateFormClass($form->id, Json::decode($form->data, true));
	    FormGenerator::generateFormView($form->id, Json::decode($form->data, true));
        return $form;
    }

    public function edit($id, FormForm $form): void
    {
        $form_entity = $this->repository->get($id);
        $form_entity->edit(
	        $form->name,
	        $this->clearJsonLabels($form->data),
	        $form->subject,
	        $form->from,
	        $form->reply,
	        $form->return,
	        $form->complete_text,
	        $form->complete_page_id,
	        $form->status,
	        $form->captcha
        );
        $this->repository->save($form_entity);
	    FormGenerator::generateFormClass($id, Json::decode($form->data, true));
	    FormGenerator::generateFormView($id, Json::decode($form->data, true));
    }

    /**
     * @param $id
     * @throws StaleObjectException
     */
    public function remove($id): void
    {
        $form = $this->repository->get($id);
        $this->repository->remove($form);
    }
}
