<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\forms\entities\Form;
use common\modules\forms\helpers\FormHelper;

/* @var $this yii\web\View */
/* @var $form Form */

$this->title                   = $form->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('forms', 'Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js2 = <<<JS2
    $("#copyEditorCodeButton").on('click', function(){
        CopyToClipboard($('#code_editor'));
    });
    
    $("#copyTemplateCodeButton").on('click', function(){
        CopyToClipboard($('#code_template'));
    });
JS2;
$this->registerJs($js2, $this::POS_READY);

$js = <<<JS
    function CopyToClipboard(selector) {
        var body = $("body");
        selector.select();
        document.execCommand("copy");
    }
JS;
$this->registerJs($js, $this::POS_HEAD);
?>
<div class="form-view">

    <p>
        <?= Html::a(Yii::t('forms', 'Forms'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $form->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $form->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-9">

            <div class="box">
                <div class="box-header with-border"><?= Yii::t('forms', 'Common') ?></div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model'      => $form,
                        'attributes' => [
                            'id',
                            'name',
                            'subject',
                            'from',
                            'reply',
                            'return',
                            [
                                'attribute' => 'status',
                                'format'    => 'raw',
                                'value'     => function (Form $form) {
                                    return FormHelper::statusLabel($form->status);
                                }
                            ],
                            'captcha',
                            'complete_page_id'
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="box">
                <div class="box-header with-border"><?= Yii::t('forms', 'Complete Message') ?></div>
                <div class="box-body">
                    <?= Yii::$app->formatter->asHtml($form->complete_text, [
                        'Attr.AllowedRel'      => ['nofollow'],
                        'HTML.SafeObject'      => true,
                        'Output.FlashCompat'   => true,
                        'HTML.SafeIframe'      => true,
                        'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
                    ]) ?>
                </div>
            </div>

        </div>

        <div class="col-md-3">
            <div class="box box-default">
                <div class="box-header with-border"><?= Yii::t('forms', 'Insert Code') ?></div>
                <div class="box-body">

                    <?php
                    $code = "<?= \common\modules\\forms\widgets\FormWidget::widget(['id' => " . $form->id . ']) ?>';
                    ?>
                    <p><?= Yii::t('forms', 'For template') ?></p>
                    <div class="input-group">
                        <?= Html::input('text', 'code_template', $code, [
                            'class' => 'form-control',
                            'id' => 'code_template',
                        ]) ?>
                        <span class="input-group-btn">
                      <button id="copyTemplateCodeButton" type="button" class="btn btn-primary btn-flat"><i class="fa fa-files-o" aria-hidden="true"></i></button>
                    </span>
                    </div>

                    <?php
                    $code = "[?= \common\modules\\forms\widgets\FormWidget::widget(['id' => " . $form->id . ']) ?]';
                    ?>
                    <p><?= Yii::t('menu', 'For editor') ?></p>
                    <div class="input-group">
                        <?= Html::input('text', 'code_editor', $code, [
                            'class' => 'form-control',
                            'id' => 'code_editor',
                        ]) ?>
                        <span class="input-group-btn">
                      <button id="copyEditorCodeButton" type="button" class="btn btn-primary btn-flat"><i class="fa fa-files-o" aria-hidden="true"></i></button>
                    </span>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
