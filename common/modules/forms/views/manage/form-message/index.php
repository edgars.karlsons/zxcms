<?php

use common\modules\forms\entities\FormMessage;
use common\modules\forms\forms\FormMessageSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\modules\forms\entities\Form;
use backend\widgets\grid\CheckBoxColumn;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel FormMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('forms', 'Messages');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
	var body = $('body');
	$('.select-on-check-all').addClass('grid_checkbox');

	body.on('change', '.grid_checkbox', function(e) {
        var button = $('#groupActions');
        if (!multiSelected()) {
            button.attr('disabled', 'disabled');
            button.addClass('disabled');
        }
        else {
            button.removeAttr('disabled');
            button.removeClass('disabled');
        }
    });
JS;
$this->registerJs($js, $this::POS_READY);

$deleteConfirmMessage = Yii::t('forms', 'Are you sure you want to delete selected messages?');
$deleteSelectedUrl = Url::to(['delete-selected']);
$unreadSelectedUrl = Url::to(['unread-selected']);
$readSelectedUrl = Url::to(['read-selected']);

$js2 = <<<JS2
	function getRows()
    {
        var ids = new Array();
        $('input[name="gridSelection[]"]:checked').each(function() {
            ids.push(this.value);
        });
        return ids;
    }

	function multiSelected() {
        var result = false;
        $('input[name="gridSelection[]"]:checked').each(function() {
            result = true;
        });
        return result;
    }
    
    function deleteSelected() {
        if (confirm('{$deleteConfirmMessage}')) {
            var ids = JSON.stringify(getRows());
            $.post( "{$deleteSelectedUrl}", { ids: ids})
            .done(function( data ) {
                document.location.reload();
            });
        }
    }
    
    function unreadSelected() {
        var ids = JSON.stringify(getRows());
        $.post( "{$unreadSelectedUrl}", { ids: ids})
        .done(function( data ) {
            document.location.reload();
        });
    }
    
    function readSelected() {
        var ids = JSON.stringify(getRows());
        $.post( "{$readSelectedUrl}", { ids: ids})
        .done(function( data ) {
            document.location.reload();
        });
    }
JS2;
$this->registerJs($js2, $this::POS_HEAD);
?>
<div class="messages-index">

	<div style="margin-bottom: 10px;">
		<div class="btn-group">
			<?= Html::a(Yii::t('forms', 'Group actions {caret}', ['caret' => '<span class="caret">']), '#', [
				'class' => 'btn btn-default dropdown-toggle disabled',
				'id' => 'groupActions',
				'disabled' => 'disabled',
				'data-toggle' => 'dropdown',
				'aria-haspopup' => 'true',
				'aria-expanded' => 'false',
			]) ?>

			<ul class="dropdown-menu">
				<li><?= Html::a(Yii::t('forms', 'Mark as read'), '#', [
						'onclick' => new JsExpression('readSelected()'),
					]) ?></li>
				<li><?= Html::a(Yii::t('forms', 'Mark as unread'), '#', [
						'onclick' => new JsExpression('unreadSelected()'),
					]) ?></li>
				<li role="separator" class="divider"></li>
				<li><?= Html::a(Yii::t('forms', 'Delete selected'), '#', [
						'onclick' => new JsExpression('deleteSelected()'),
					]) ?></li>
			</ul>
		</div>
	</div>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
	                [
		                'class' => CheckBoxColumn::class,
	                ],
                	[
                		'attribute' => 'id',
		                'options' => ['style' => 'width: 50px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                	[
                		'label' => Yii::t('forms', 'Date'),
                		'attribute' => 'created_at',
		                'value' => function(FormMessage $model) {
            	            return date('d.m.Y H:i', $model->created_at);
		                },
		                'options' => ['style' => 'width: 100px;'],
	                ],
                    [
	                    'label' => Yii::t('forms', 'Form'),
	                    'filter' => ArrayHelper::map(Form::find()->all(), 'id', 'name'),
                        'attribute' => 'form_id',
                        'value' => function (FormMessage $model) {
                            return Html::a(Html::encode($model->form->name), ['view', 'id' => $model->id], [
                            	'style' => $model->new == 1 ? 'font-weight: bold' : '',
                            ]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => ActionColumn::class,
	                    'template' => '{view} {delete}',
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
