<?php

use yii\helpers\Html;
use common\modules\forms\entities\FormMessage;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $message FormMessage */

$this->title = $message->form->name . ': ' . date('d.m.Y H:i', $message->created_at);
$this->params['breadcrumbs'][] = ['label' => Yii::t('forms', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$data = Json::decode($message->data, true);
?>
<div class="message-view">

    <p>
	    <?= Html::a(Yii::t('forms','Messages'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $message->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

	<div class="row">
		<div class="col-md-12">

		    <div class="box">
		        <div class="box-body">
			        <table class="table">
				        <tbody>
				            <?php foreach ($data as $item): ?>
					            <tr>
						            <td>
							            <?= $item['key'] ?>
						            </td>
						            <td>
							            <?= Yii::$app->formatter->asNtext($item['value']) ?>
						            </td>
					            </tr>
					        <?php endforeach; ?>
				        </tbody>
			        </table>
		        </div>
		    </div>

		    <!-- <div class="box">
		        <div class="box-header with-border">< ?= Yii::t('pages', 'Complete Message') ?></div>
		        <div class="box-body">
		            < ?= Yii::$app->formatter->asHtml($form->complete_text, [
		                'Attr.AllowedRel' => array('nofollow'),
		                'HTML.SafeObject' => true,
		                'Output.FlashCompat' => true,
		                'HTML.SafeIframe' => true,
		                'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
		            ]) ?>
		        </div>
		    </div> -->

		</div>

	</div>

</div>
