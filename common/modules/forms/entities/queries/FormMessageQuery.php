<?php
/**
 * Created by Error202
 * Date: 27.07.2018
 */

namespace common\modules\forms\entities\queries;

use common\modules\forms\entities\FormMessage;
use yii\db\ActiveQuery;

class FormMessageQuery extends ActiveQuery
{
	public function unread()
	{
		return $this->andWhere([
			'new' => FormMessage::STATUS_NEW,
		]);
	}
}