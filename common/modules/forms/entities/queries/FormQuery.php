<?php
/**
 * Created by Error202
 * Date: 27.07.2018
 */

namespace common\modules\forms\entities\queries;


use common\modules\forms\entities\Form;
use yii\db\ActiveQuery;

class FormQuery extends ActiveQuery
{
	public function active()
	{
		return $this->andWhere([
			'active' => Form::STATUS_ACTIVE,
		]);
	}
}