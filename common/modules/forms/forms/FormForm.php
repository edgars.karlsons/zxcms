<?php

namespace common\modules\forms\forms;

use common\modules\forms\entities\Form;
use yii\base\Model;
use Yii;
use yii\helpers\Json;

class FormForm extends Model
{
    public ?string $name = null;
    public ?string $data = null;
    public ?string $subject = null;
    public ?string $from = null;
    public ?string $reply = null;
    public ?string $return = null;
    public ?string $complete_text = null;
    public ?int $complete_page_id = null;
    public ?int $status = null;
    public ?int $captcha = null;

    public Form $form;

    public function __construct(Form $form = null, $config = [])
    {
        if ($form) {
            $this->name = $form->name;
            $this->data = $form->data;
			$this->subject = $form->subject;
			$this->from = $form->from;
			$this->reply = $form->reply;
			$this->return = $form->return;
			$this->complete_text = $form->complete_text;
			$this->complete_page_id = $form->complete_page_id;
			$this->status = $form->status;
			$this->captcha = $form->captcha;

            $this->form = $form;
        }
        else {
        	$this->data = Json::encode([]);
        	$this->status = Form::STATUS_ACTIVE;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'data'], 'required'],
            [['status', 'captcha', 'complete_page_id'], 'integer'],
            [['name', 'subject'], 'string', 'max' => 255],
	        [['from', 'reply', 'return'], 'email'],
            [['data', 'complete_text'], 'string'],
        ];
    }

	public function attributeLabels(): array
    {
		return [
			'name' => Yii::t('forms', 'Name'),
			'data' => Yii::t('forms', 'Form'),
			'subject' => Yii::t('forms', 'Subject'),
			'from' => Yii::t('forms', 'From E-mail'),
			'reply' => Yii::t('forms', 'Reply E-mail'),
			'return' => Yii::t('forms', 'Return E-mail'),
			'complete_text' => Yii::t('forms', 'Complete Text'),
			'complete_page_id' => Yii::t('forms', 'Complete Page'),
			'status' => Yii::t('forms', 'Status'),
			'captcha' => Yii::t('forms', 'Use Captcha'),
		];
	}
}
