<?php

namespace common\modules\forms\forms;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\forms\entities\Form;

class FormSearch extends Model
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $slug = null;
    public ?string $subject = null;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 'slug', 'subject'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Form::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'subject', $this->subject]);
        return $dataProvider;
    }
}
