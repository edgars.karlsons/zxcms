<?php

namespace common\modules\languages\forms;

use common\modules\languages\entities\Language;
use yii\base\Model;
use Yii;

class LanguageForm extends Model
{
	public ?string $name = null;
    public ?string $title = null;
    public ?int $status = null;

    public Language $language;

    public function __construct(Language $language = null, $config = [])
    {
        if ($language) {
        	$this->name = $language->name;
            $this->title = $language->title;
            $this->status = $language->status;

            $this->language = $language;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title', 'name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 2],
	        [['title'], 'string', 'max' => 64],
        ];
    }

	public function attributeLabels(): array
    {
		return [
			'id' => Yii::t('languages', 'ID'),
			'name' => Yii::t('languages', 'Name'),
			'title' => Yii::t('languages', 'Title'),
			'status' => Yii::t('languages', 'Status'),
			'default' => Yii::t('languages', 'Default'),
		];
	}
}
