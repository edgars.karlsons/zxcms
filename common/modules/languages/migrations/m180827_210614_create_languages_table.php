<?php

use yii\db\Migration;

/**
 * Handles the creation of table `languages`.
 */
class m180827_210614_create_languages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%languages}}', [
            'id'      => $this->primaryKey(),
            'name'    => $this->string(2)->notNull(),
            'title'   => $this->string(64)->notNull(),
            'status'  => $this->integer(1)->defaultValue(1),
            'default' => $this->integer(1)->defaultValue(0),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%languages}}');
    }
}
