<?php
/**
 * Created by Error202
 * Date: 21.08.2018
 */

namespace common\modules\languages\helpers;

use common\modules\languages\entities\Language;
use Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class LanguageHelper
{
	public static function defaultLabel($default): string
	{
        $class = match ($default) {
            Language::DEFAULT_FALSE => 'text-gray',
            Language::DEFAULT_TRUE => 'text-yellow',
            default => 'text-gray',
        };

		return '<i class="fa fa-star '.$class.'" aria-hidden="true"></i>';
	}

	public static function statusList(): array
	{
		return [
			Language::STATUS_DRAFT => Yii::t('languages', 'Draft'),
			Language::STATUS_ACTIVE => Yii::t('languages', 'Active'),
		];
	}

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
	public static function statusName($status): string
	{
		return ArrayHelper::getValue(self::statusList(), $status);
	}

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
	public static function statusLabel($status): string
	{
        $class = match ($status) {
            Language::STATUS_DRAFT => 'label label-default',
            Language::STATUS_ACTIVE => 'label label-success',
            default => 'label label-default',
        };

		return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
			'class' => $class,
		]);
	}
}
