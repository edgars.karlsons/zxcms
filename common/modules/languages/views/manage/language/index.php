<?php

use common\modules\languages\forms\search\LanguageSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\languages\entities\Language;
use common\modules\languages\helpers\LanguageHelper;

/* @var $this yii\web\View */
/* @var $searchModel LanguageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('languages', 'Languages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-index">

    <div class="card">
        <div class="card-header">
            <?= Html::a(Yii::t('languages','Create Language'), ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                	[
                		'attribute' => 'default',
		                'format' => 'raw',
		                'value' => function(Language $language) {
            	            return Html::a(LanguageHelper::defaultLabel($language->default), ['set-default', 'id' => $language->id], [
		                        'data' => [
			                        'confirm' => Yii::t('languages', 'Are you sure you want to set this language as default?'),
			                        'method' => 'post',
		                        ],
	                        ]);
		                },
		                'options' => ['style' => 'width: 150px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                    [
                        'attribute' => 'title',
                        'value' => function (Language $model) {
                            return Html::a(Html::encode($model->title), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    'name',
	                [
		                'attribute' => 'status',
		                'filter' => LanguageHelper::statusList(),
		                'format' => 'raw',
		                'value' => function(Language $language) {
			                return LanguageHelper::statusLabel($language->status);
		                },
		                'options' => ['style' => 'width: 150px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                    [
                        'class' => ActionColumn::class,
	                    'visibleButtons' => [
					        'delete' => function ($model) {
					            return !($model->default === 1);
					         }
					    ],
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
