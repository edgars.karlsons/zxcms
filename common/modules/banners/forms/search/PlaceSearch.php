<?php

namespace common\modules\banners\forms\search;

use common\modules\banners\entities\BannerPlace;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PlaceSearch extends Model
{
    public ?int $id = null;
    public ?string $title = null;
    public ?int $width = null;
    public ?int $height = null;
    public ?int $active = null;

    public function rules(): array
    {
        return [
            [['id', 'active', 'width', 'height'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = BannerPlace::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'active' => $this->active,
            'width'  => $this->width,
            'height' => $this->height
        ]);

        $query
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
