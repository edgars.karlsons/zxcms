<?php

namespace common\modules\banners\forms\search;

use common\modules\banners\entities\Banner;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class BannerSearch extends Model
{
    public ?int $id = null;
    public ?string $title = null;
    public ?int $active = null;
    public ?int $place_id = null;
    public ?string $target = null;

    public function rules(): array
    {
        return [
            [['id', 'active', 'place_id'], 'integer'],
            [['title', 'target'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Banner::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'       => $this->id,
            'active'   => $this->active,
            'place_id' => $this->place_id,
            'target'   => $this->target
        ]);

        $query
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
