<?php

namespace common\modules\banners\forms;

use common\modules\banners\entities\Banner;
use yii\base\Model;
use Yii;
use yii\web\UploadedFile;

class BannerForm extends Model
{
    public ?string $title = null;

    public string|null|UploadedFile $image = null;
    public ?string $url = null;
    public ?string $target = null;
    public ?int $active = null;
    public ?int $start_at = null;
    public ?int $end_at = null;
    public ?string $include_urls = null;
    public ?string $exclude_urls = null;
    public ?int $place_id = null;

    public Banner $banner;

    public function __construct(Banner $banner = null, $config = [])
    {
        if ($banner) {
            $this->title        = $banner->title;
            $this->image        = $banner->image;
            $this->url          = $banner->url;
            $this->target       = $banner->target;
            $this->active       = $banner->active;
            $this->start_at     = $banner->start_at;
            $this->end_at       = $banner->end_at;
            $this->include_urls = $banner->include_urls;
            $this->exclude_urls = $banner->exclude_urls;
            $this->place_id     = $banner->place_id;
            $this->banner      = $banner;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title', 'place_id', 'start_at', 'end_at'], 'required'],
            [['active', 'place_id', 'active'], 'integer'],
            [['title', 'target', 'url'], 'string', 'max' => 255],
            [['include_urls', 'exclude_urls'], 'string'],
            [['image'], 'file', 'extensions' => 'png, jpg, gif'],
            [['start_at', 'end_at'], 'safe'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id'           => Yii::t('banners', 'ID'),
            'title'        => Yii::t('banners', 'Title'),
            'url'          => Yii::t('banners', 'URL'),
            'image'        => Yii::t('banners', 'Image'),
            'target'       => Yii::t('banners', 'Target'),
            'active'       => Yii::t('banners', 'Status'),
            'start_at'     => Yii::t('banners', 'Start At'),
            'end_at'       => Yii::t('banners', 'End At'),
            'created_at'   => Yii::t('banners', 'Created At'),
            'updated_at'   => Yii::t('banners', 'Updated At'),
            'include_urls' => Yii::t('banners', 'Show only on URLs'),
            'exclude_urls' => Yii::t('banners', 'Not show on URLs'),
            'views'        => Yii::t('banners', 'Views'),
            'clicks'       => Yii::t('banners', 'Visits'),
            'place_id'     => Yii::t('banners', 'Place'),
        ];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
            $this->image    = UploadedFile::getInstance($this, 'image');
            $this->start_at = strtotime($this->start_at);
            $this->end_at   = strtotime($this->end_at);

            return true;
        }

        return false;
    }
}
