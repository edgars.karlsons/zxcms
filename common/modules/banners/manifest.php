<?php

return [
    'version'     => '1.0.1',
    'name'        => 'banners',
    'description' => 'Banners management and rotation system for site',
    'module'      => 'BannersModule',
    'permissions' => [
        'BannersManagement' => 'Manage banners system',
    ],
];
