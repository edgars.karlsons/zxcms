<?php

namespace common\modules\banners\entities;

use common\modules\banners\entities\queries\BannerPlaceQuery;
use DomainException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property int $id
 * @property string $title
 * @property int $width
 * @property int $height
 * @property int $active
 *
 *
 * @property Banner[] $banners
 * @property Banner[] $activeBanners
 */
class BannerPlace extends ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    public static function create($title, $width, $height, $active): self
    {
        $place         = new static();
        $place->title  = $title;
        $place->width  = $width;
        $place->height = $height;
        $place->active = $active;

        return $place;
    }

    public function edit($title, $width, $height, $active): void
    {
        $this->title  = $title;
        $this->width  = $width;
        $this->height = $height;
        $this->active = $active;
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'banners_places';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'     => Yii::t('banners', 'ID'),
            'title'  => Yii::t('banners', 'Title'),
            'width'  => Yii::t('banners', 'Width'),
            'height' => Yii::t('banners', 'Height'),
            'active' => Yii::t('banners', 'Status'),
        ];
    }

    public function activate(): void
    {
        if ($this->isActive()) {
            throw new DomainException('Place is already active.');
        }
        $this->active = self::STATUS_ACTIVE;
    }

    public function draft(): void
    {
        if ($this->isDraft()) {
            throw new DomainException('Place is already draft.');
        }
        $this->active = self::STATUS_DRAFT;
    }

    public function isActive(): bool
    {
        return $this->active == self::STATUS_ACTIVE;
    }

    public function isDraft(): bool
    {
        return $this->active == self::STATUS_DRAFT;
    }

    public function getBanners(): ActiveQuery
    {
        return $this->hasMany(Banner::class, ['place_id' => 'id']);
    }

    public function getActiveBanners(): ActiveQuery
    {
        return $this->hasMany(Banner::class, ['place_id' => 'id'])->andWhere(['active' => Banner::STATUS_ACTIVE]);
    }

    public static function find(): BannerPlaceQuery
    {
        return new BannerPlaceQuery(static::class);
    }
}
