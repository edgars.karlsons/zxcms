<?php
/**
 * Created by Error202
 * Date: 21.08.2018
 */

namespace common\modules\banners\helpers;

use common\modules\banners\entities\Banner;
use Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class BannerHelper
{
    public static function targetList(): array
    {
        return [
            Banner::TARGET_SELF  => Yii::t('banners', 'Self window'),
            Banner::TARGET_BLANK => Yii::t('banners', 'Blank window'),
        ];
    }

    /**
     * @param $target
     * @return string
     * @throws Exception
     */
    public static function targetName($target): string
    {
        return ArrayHelper::getValue(self::targetList(), $target);
    }

    public static function statusList(): array
    {
        return [
            Banner::STATUS_DRAFT  => Yii::t('banners', 'Draft'),
            Banner::STATUS_ACTIVE => Yii::t('banners', 'Active'),
        ];
    }

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
    public static function statusLabel($status): string
    {
        $class = match ($status) {
            Banner::STATUS_DRAFT => 'label label-default',
            Banner::STATUS_ACTIVE => 'label label-success',
            default => 'label label-default',
        };

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}
