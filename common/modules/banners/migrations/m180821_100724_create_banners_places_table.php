<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banners_places`.
 */
class m180821_100724_create_banners_places_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%banners_places}}', [
            'id'     => $this->primaryKey(),
            'title'  => $this->string(255)->notNull(),
            'width'  => $this->integer(),
            'height' => $this->integer(),
            'active' => $this->integer(1)->defaultValue(1)
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%banners_places}}');
    }
}
