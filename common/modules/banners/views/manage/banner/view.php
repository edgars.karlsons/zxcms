<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\banners\entities\Banner;
use common\modules\banners\helpers\BannerHelper;

/* @var $this yii\web\View */
/* @var $banner Banner */

$this->title = $banner->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('banners', 'Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-view">

    <p>
	    <?= Html::a(Yii::t('banners','Banners'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $banner->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $banner->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

	<div class="row">
		<div class="col-md-12">

			<?php if ($banner->image): ?>
			<div class="box">
				<div class="box-body">
					<?= Html::img('@static/origin/banners/' . $banner->image, [
						'style' => 'margin: 20px'
					]) ?>
				</div>
			</div>
			<?php endif; ?>

		    <div class="box">
		        <div class="box-body">
		            <?= DetailView::widget([
		                'model' => $banner,
		                'attributes' => [
		                    'id',
		                    'title',
			                [
			                	'attribute' => 'place.title',
				                'label' => Yii::t('banners', 'Place'),
			                ],
			                'url:url',
			                [
			                	'attribute' => 'target',
				                'value' => function(Banner $banner) {
		            	            return BannerHelper::targetName($banner->target);
				                }
			                ],
			                [
				                'attribute' => 'start_at',
				                'value' => function(Banner $banner) {
					                return date('d.m.Y H:i', $banner->start_at);
				                },
			                ],
			                [
				                'attribute' => 'end_at',
				                'value' => function(Banner $banner) {
					                return date('d.m.Y H:i', $banner->end_at);
				                },
			                ],
			                [
				                'attribute' => 'created_at',
				                'value' => function(Banner $banner) {
					                return date('d.m.Y H:i', $banner->created_at);
				                },
			                ],
			                [
				                'attribute' => 'updated_at_at',
				                'value' => function(Banner $banner) {
					                return date('d.m.Y H:i', $banner->updated_at);
				                },
			                ],
			                'views',
			                'clicks'
		                ],
		            ]) ?>
		        </div>
		    </div>

		</div>

	</div>

</div>
