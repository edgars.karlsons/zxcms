<?php

use common\modules\banners\forms\BannerForm;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use common\modules\banners\helpers\BannerHelper;
use common\modules\banners\entities\BannerPlace;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model BannerForm */
/* @var $form yii\widgets\ActiveForm */

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-10">

            <div class="box box-default">
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'place_id')->dropDownList(ArrayHelper::map(BannerPlace::find()->all(), 'id', 'title')) ?>
                        </div>
                        <div class="col-md-8">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'target')->dropDownList(BannerHelper::targetList()) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'image')->widget(FileInput::class, [
                                'options'       => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showUpload'  => false,
                                    'showPreview' => false,
                                ],
                            ]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'start_at')->widget(DateTimePicker::class, [
                                'options'       => [],
                                'removeButton'  => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format'    => 'dd.mm.yyyy hh:ii:ss',
                                ]
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'end_at')->widget(DateTimePicker::class, [
                                'options'       => [],
                                'removeButton'  => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    //'format' => 'dd.MM.yyyy hh:i',
                                    'format'    => 'dd.mm.yyyy hh:ii:ss',
                                ]
                            ]); ?>
                        </div>
                    </div>

                    <!--<div class="row">
                        <div class="col-md-6">
                            < ?= $form->field($model, 'include_urls')->textarea(['rows' => 4]) ?>
                        </div>
                        <div class="col-md-6">
                            < ?= $form->field($model, 'exclude_urls')->textarea(['rows' => 4]) ?>
                        </div>
                    </div>-->

                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

        </div>

        <div class="col-md-2">
            <div class="box box-default">
                <div class="box-header with-border"><?= Yii::t('banners', 'Publish') ?></div>
                <div class="box-body">

                    <?= $form->field($model, 'active')->radioList(BannerHelper::statusList()) ?>

                </div>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
