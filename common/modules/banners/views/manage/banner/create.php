<?php

use common\modules\banners\forms\BannerForm;

/* @var $this yii\web\View */
/* @var $model BannerForm */

$this->title = Yii::t('banners', 'Create Banner');
$this->params['breadcrumbs'][] = ['label' => Yii::t('banners', 'Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
