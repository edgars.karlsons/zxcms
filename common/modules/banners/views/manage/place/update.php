<?php

/* @var $this yii\web\View */
/* @var $place \common\modules\banners\entities\BannerPlace */
/* @var $model \common\modules\banners\forms\BannerPlaceForm */

$this->title = Yii::t('banners', 'Update Place: {name}', ['name' => $place->title]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('banners', 'Places'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $place->title, 'url' => ['view', 'id' => $place->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="place-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
