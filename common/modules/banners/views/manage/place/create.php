<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\banners\forms\BannerPlaceForm */

$this->title = Yii::t('banners', 'Create Place');
$this->params['breadcrumbs'][] = ['label' => Yii::t('banners', 'Places'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="place-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
