<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use common\modules\banners\helpers\BannerHelper;

/* @var $this yii\web\View */
/* @var $model \common\modules\banners\forms\BannerPlaceForm */
/* @var $form yii\widgets\ActiveForm */

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="place-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-10">

		    <div class="card">
		        <div class="card-body">
		            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

		            <div class="row">
			            <div class="col-md-6">
				            <?= $form->field($model, 'width')->textInput(['maxlength' => true]) ?>
			            </div>
			            <div class="col-md-6">
				            <?= $form->field($model, 'height')->textInput(['maxlength' => true]) ?>
			            </div>
		            </div>


		        </div>
                <div class="card-footer text-right">
                        <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
		    </div>




		</div>

		<div class="col-md-2">
			<div class="card">
				<div class="card-header"><?= Yii::t('banners', 'Publish') ?></div>
				<div class="card-body">

					<?= $form->field($model, 'active')->radioList(BannerHelper::statusList()) ?>

				</div>
			</div>
		</div>

	</div>

    <?php ActiveForm::end(); ?>

</div>
