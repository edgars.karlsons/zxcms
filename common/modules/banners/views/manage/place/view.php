<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\banners\entities\BannerPlace;
use common\modules\banners\helpers\BannerHelper;

/* @var $this yii\web\View */
/* @var $place \common\modules\banners\entities\BannerPlace */

$this->title                   = $place->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('banners', 'Places'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js2 = <<<JS2
    $("#copyEditorCodeButton").on('click', function(){
        CopyToClipboard($('#code_editor'));
    });
    
    $("#copyTemplateCodeButton").on('click', function(){
        CopyToClipboard($('#code_template'));
    });
JS2;
$this->registerJs($js2, $this::POS_READY);

$js = <<<JS
    function CopyToClipboard(selector) {
        var body = $("body");
        selector.select();
        document.execCommand("copy");
    }
JS;
$this->registerJs($js, $this::POS_HEAD);
?>
<div class="place-view">

    <p>
        <?= Html::a(Yii::t('banners', 'Places'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $place->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $place->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-9">

            <div class="box">
                <div class="box-body">
                    <?= DetailView::widget([
                        'model'      => $place,
                        'attributes' => [
                            'id',
                            'title',
                            'width',
                            'height',
                            [
                                'attribute' => 'active',
                                'format'    => 'raw',
                                'value'     => function (BannerPlace $place) {
                                    return BannerHelper::statusLabel($place->active);
                                }
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="box box-default">
                <div class="box-header with-border"><?= Yii::t('banners', 'Insert Code') ?></div>
                <div class="box-body">

                    <?php
                    $code = "<?= \common\modules\\banners\widgets\BannerWidget::widget(['id' => " . $place->id . ']) ?>';
                    ?>
                    <p><?= Yii::t('banners', 'For template') ?></p>
                    <div class="input-group">
                        <?= Html::input('text', 'code_template', $code, [
                            'class' => 'form-control',
                            'id' => 'code_template',
                        ]) ?>
                        <span class="input-group-btn">
                      <button id="copyTemplateCodeButton" type="button" class="btn btn-primary btn-flat"><i class="fa fa-files-o" aria-hidden="true"></i></button>
                    </span>
                    </div>

                    <?php
                    $code = "[?= \common\modules\\banners\widgets\BannerWidget::widget(['id' => " . $place->id . ']) ?]';
                    ?>
                    <p><?= Yii::t('banners', 'For editor') ?></p>
                    <div class="input-group">
                        <?= Html::input('text', 'code_editor', $code, [
                            'class' => 'form-control',
                            'id' => 'code_editor',
                        ]) ?>
                        <span class="input-group-btn">
                      <button id="copyEditorCodeButton" type="button" class="btn btn-primary btn-flat"><i class="fa fa-files-o" aria-hidden="true"></i></button>
                    </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
