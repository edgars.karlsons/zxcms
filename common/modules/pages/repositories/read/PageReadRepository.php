<?php

namespace common\modules\pages\repositories\read;

use common\modules\pages\entities\Page;

class PageReadRepository
{
    public function getAll(): array
    {
        return Page::find()->typePublic()->andWhere(['>', 'depth', 0])->all();
    }

    public function find($id): ?Page
    {
        return Page::findOne($id);
    }

    public function findBySlug($slug): ?Page
    {
        return Page::find()->typePublic()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }

	public function findPreview($id, $language): ?Page
	{
		return $language ? Page::find()->localized($language)->andWhere(['id' => $id])->one() : Page::find()->andWhere(['id' => $id])->one();
	}
}
