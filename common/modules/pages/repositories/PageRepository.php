<?php

namespace common\modules\pages\repositories;

use common\modules\pages\entities\Page;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class PageRepository
{
    public function get($id): Page
    {
	    if (!$page = Page::find()->with('translations')->andWhere(['id' => $id])->one()) {
            throw new NotFoundException('Page is not found.');
        }
        return $page;
    }

    public function save(Page $page): void
    {
        if (!$page->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param Page $page
     * @throws StaleObjectException
     */
    public function remove(Page $page): void
    {
        if (!$page->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
