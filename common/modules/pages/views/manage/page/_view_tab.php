<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\web\View;
use yii\widgets\DetailView;
use common\modules\pages\entities\Page;

/**
 * @var $this View
 * @var $page Page
 * @var $language string
 */

?>

<?= DetailView::widget([
	'model' => $page,
	'attributes' => [
		[
			'label' => Yii::t('pages', 'Title'),
			'value' => function(Page $entity) use ($language) {
				return $entity->findTranslation($language)->title;
			}
		],
	],
]) ?>

<h6><?= Yii::t('pages', 'SEO') ?></h6>


<?= DetailView::widget([
	'model' => $page,
	'attributes' => [
		[
			'label' => Yii::t('pages', 'META Title'),
			'value' => function(Page $entity) use ($language) {
				return $entity->findTranslation($language)->meta_title;
			}
		],
		[
			'label' => Yii::t('pages', 'META Description'),
			'value' => function(Page $entity) use ($language) {
				return $entity->findTranslation($language)->meta_description;
			}
		],
		[
			'label' => Yii::t('pages', 'META Keywords'),
			'value' => function(Page $entity) use ($language) {
				return $entity->findTranslation($language)->meta_keywords;
			}
		],
	],
]) ?>

<div class="card">
    <div class="card-header">
        <?= Yii::t('pages', 'Content') ?>
    </div>
    <div class="card-body">
        <?= Yii::$app->formatter->asHtml($page->findTranslation($language)->content, [
            'Attr.AllowedRel' => array('nofollow'),
            'HTML.SafeObject' => true,
            'Output.FlashCompat' => true,
            'HTML.SafeIframe' => true,
            'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
        ]) ?>
    </div>
</div>
