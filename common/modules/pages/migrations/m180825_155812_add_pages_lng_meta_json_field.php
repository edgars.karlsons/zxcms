<?php

use yii\db\Migration;

/**
 * Class m180825_155812_add_pages_lng_meta_json_field
 */
class m180825_155812_add_pages_lng_meta_json_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%pages_lng}}', 'meta_json', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pages_lng}}', 'meta_json');
    }
}
