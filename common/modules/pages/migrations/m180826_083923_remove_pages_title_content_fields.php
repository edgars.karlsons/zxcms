<?php

use yii\db\Migration;

/**
 * Class m180826_083923_remove_pages_title_content_fields
 */
class m180826_083923_remove_pages_title_content_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->dropColumn('{{%pages}}', 'title');
	    $this->dropColumn('{{%pages}}', 'content');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%pages}}', 'title', $this->string(255)->notNull());
	    $this->addColumn('{{%pages}}', 'title', 'MEDIUMTEXT');
    }
}
