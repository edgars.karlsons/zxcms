<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

/**
 * @var $this View
 * @var $model MenuItemForm
 */

use core\forms\menu\MenuItemForm;
use kartik\widgets\Select2;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;

$block_name = Yii::t('pages', 'Pages');
$block_title_attr = Yii::t('pages', 'Pages');

$js = <<<JS
	function updatePagesUrl() {
	    var select = $("#page_select");
	    var data = select.select2('data');
	    var selected_post = select.val();
	    $("#page_menu_item_url").val('/pages/page/view/');
	    $("#page_menu_item_url_params").val('{"id":'+selected_post+'}');
	    $("#page_menu_item_name").val(data[0].text);
	    $("#page_menu_item_title_attr").val(data[0].text);
	}
JS;
$this->registerJs($js, $this::POS_HEAD);

$fetchUrl = Url::to( [ '/pages/manage/page/page-search' ] );
?>

<div class="menu_item_widget">

	<div class="form-group">
		<div>
			<?= Select2::widget([
				'name' => 'page_select',
			    'value' => '',
			    'options' => [
			    	'placeholder' => Yii::t('pages', 'Select page...'),
				    'id' => 'page_select',
				    'onchange' => new JsExpression("updatePagesUrl()"),
			    ],
				'pluginOptions' => [
					'ajax' => [
						'url' => $fetchUrl,
						'dataType' => 'json',
						'data' => new JsExpression('function(params) { return {q:params.term}; }')
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
					'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
				],
			]) ?>
		</div>
	</div>

<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')->hiddenInput([
		'id' => 'page_menu_item_name',
	])->label(false) ?>

	<?= $form->field($model, 'title_attr')->hiddenInput([
		'id' => 'page_menu_item_title_attr',
	])->label(false) ?>

	<?= $form->field($model, 'module')->hiddenInput([
	])->label(false) ?>

	<?= $form->field($model, 'menu_id')->hiddenInput([
	])->label(false) ?>

	<?= $form->field($model, 'url')->hiddenInput([
		'id' => 'page_menu_item_url',
	])->label(false) ?>

	<?= $form->field($model, 'url_params')->hiddenInput([
		'value' => '',
		'id' => 'page_menu_item_url_params',
	])->label(false) ?>

<div class="form-group">
<?= Html::submitButton(Yii::t('buttons', 'Add to menu'), [
	'class' => 'btn btn-info btn-sm float-right'
]) ?>
</div>

<?php ActiveForm::end(); ?>

</div>