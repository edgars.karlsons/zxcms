<?php
/**
 * Created by Error202
 * Date: 27.07.2018
 */

namespace common\modules\pages\entities\queries;

use common\modules\pages\entities\Page;
use yii\db\ActiveQuery;
use core\components\LanguageTranslateTrait;

class PageQuery extends ActiveQuery
{
	use LanguageTranslateTrait;

	public function typePublic($alias = null): PageQuery
    {
		return $this->andWhere([
			($alias ? $alias . '.' : '') . 'type' => Page::TYPE_PUBLIC,
		]);
	}
}
