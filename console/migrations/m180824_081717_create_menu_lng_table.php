<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu_lng`.
 */
class m180824_081717_create_menu_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%menu_lng}}', [
            'id'       => $this->primaryKey(),
            'menu_id'  => $this->integer()->notNull(),
            'language' => $this->string(6)->notNull(),
            'name'     => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_menu_lng_language', '{{%menu_lng}}', 'language');
        $this->createIndex('idx_menu_lng_menu_id', '{{%menu_lng}}', 'menu_id');
        $this->addForeignKey('fk_menu_lng_menu_id', '{{%menu_lng}}', 'menu_id', '{{%menu}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_menu_lng_menu_id', '{{%menu_lng}}');
        $this->dropColumn('idx_menu_lng_menu_id', '{{%menu_lng}}');
        $this->dropColumn('idx_menu_lng_language', '{{%menu_lng}}');
        $this->dropTable('{{%menu_lng}}');
    }
}
