<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_categories`.
 */
class m180110_121335_create_post_categories_table extends Migration
{
	public function up()
	{
		$tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

		$this->createTable('{{%post_categories}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'slug' => $this->string()->notNull(),
			'title' => $this->string(),
			'description' => $this->text(),
			'sort' => $this->integer()->notNull(),
			'meta_json' => 'LONGTEXT NOT NULL',
			'type_id' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createIndex('{{%idx-post_categories-slug}}', '{{%post_categories}}', 'slug', true);
	}

	public function down()
	{
		$this->dropTable('{{%post_categories}}');
	}
}
