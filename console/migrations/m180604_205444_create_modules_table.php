<?php

use yii\db\Migration;

/**
 * Handles the creation of table `modules`.
 */
class m180604_205444_create_modules_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
	    }
	    $this->createTable(
		    '{{%modules}}',
		    [
			    'id' => $this->primaryKey(),
			    'name' => $this->string(255)->notNull()->unique(),
			    'class' => $this->string(255)->notNull(),
			    'type' => $this->string(255)->notNull(), // frontend, backend, common
                'system' => $this->integer(1)->defaultValue(0),
			    'active' => $this->integer(1)->defaultValue(1),
		    ],
		    $tableOptions
	    );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%modules}}');
    }
}
