<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_tags`.
 */
class m180110_121109_create_post_tags_table extends Migration
{
	public function up()
	{
		$tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

		$this->createTable('{{%post_tags}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'slug' => $this->string()->notNull(),
			'type_id' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createIndex('{{%idx-post_tags-slug}}', '{{%post_tags}}', 'slug', true);
		$this->createIndex('{{%idx-post_tags-type_id}}', '{{%post_tags}}', 'type_id');
		$this->addForeignKey('{{%fk-post_tags-type_id_id}}', '{{%post_tags}}', 'type_id', '{{%post_types}}', 'id');
	}

	public function down()
	{
		$this->dropTable('{{%post_tags}}');
	}
}
