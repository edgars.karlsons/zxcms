<?php
/**
 * Created by PhpStorm.
 * User: Egorka
 * Date: 11.08.2017
 * Time: 15:07
 */

namespace console\controllers;

use core\forms\user\UserForm;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use core\services\user\UserManageService;
use yii\console\Controller;
use Yii;

class UserController extends Controller
{
    private UserManageService $service;

    public function __construct($id, $module, UserManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function actionAdd()
    {
        $username = $this->prompt('Username:', ['required' => true]);
        $email    = $this->prompt('Email:', ['required' => true]);
        $password = $this->prompt('Password:', ['required' => true]);
        $role     = $this->select('Role:', ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'));

        $form           = new UserForm();
        $form->username = $username;
        $form->email    = $email;
        $form->password = $password;
        $form->role     = $role;

        if ($form->validate()) {
            $this->service->create($form);
            $this->stdout('Done!' . PHP_EOL);

            return;
        }

        $this->stdout('Errors found!' . PHP_EOL);
        foreach ($form->errors as $error) {
            $this->stdout(is_string($error) ? $error : $error[0] . PHP_EOL);
        }
    }

    /**
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function actionAddAdmin($username, $email, $password)
    {
        $form           = new UserForm();
        $form->username = $username;
        $form->email    = $email;
        $form->password = $password;
        $form->role     = 'admin';

        if ($form->validate()) {
            $this->service->create($form);
        }
    }
}
