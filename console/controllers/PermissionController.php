<?php
/**
 * Created by Error202
 * Date: 04.09.2018
 */

namespace console\controllers;

use core\services\PermissionManager;
use yii\console\Controller;

/**
 * Permissions management from console
 * Class PermissionController
 * @package console\controllers
 */
class PermissionController extends Controller
{
    /**
     * @var PermissionManager Permissions management service
     */
    private PermissionManager $service;

    public function __construct(string $id, $module, PermissionManager $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * Create permission
     * @param $name
     * @param null $description
     */
    public function actionAdd($name, $description = null) : void
    {
        $this->service->create($name, $description);
    }
}
