<?php

namespace backend\widgets\dashboard\templates;

use yii\bootstrap\Widget;

class AdminLteSimpleBox1 extends Widget
{
    public $bgclass;
    public $cssclass;
    public $icon;
    public $title;
    public $subtitle;

    public function init()
    {
        parent::init();

        $this->bgclass  = $this->bgclass === null ? 'bg-aqua' : $this->bgclass;
        $this->cssclass = $this->cssclass === null ? 'col-md-3 col-sm-6 col-xs-12' : $this->cssclass;
        $this->icon     = $this->icon === null ? 'fa fa-envelope-o' : $this->icon;
        $this->title    = $this->title === null ? 'Messages' : $this->title;
        $this->subtitle = $this->subtitle === null ? '0' : $this->subtitle;
    }

    public function run()
    {
        return '<div class="' . $this->cssclass . '">
            <div class="info-box">
                <span class="info-box-icon ' . $this->bgclass . '">
                    <i class="' . $this->icon . '"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">' . $this->title . '</span>
                    <span class="info-box-number">' . $this->subtitle . '</span>
                </div>
            </div>
        </div>';
    }
}
