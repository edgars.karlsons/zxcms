<?php

use yii\helpers\Url;

/**
 * @var $this \yii\web\View;
 * @var $users \core\entities\user\User[]
 */
?>

<div class="box-body no-padding">
    <ul class="users-list clearfix">

        <?php foreach ($users as $user) : ?>

            <li>
                <img class="user128" src="<?= Yii::$app->avatar->show($user->username) ?>" alt="<?= $user->username ?>">
                <a class="users-list-name"
                   href="<?= Url::toRoute(['user-manage/view', 'id' => $user->id]) ?>"><?= $user->username ?></a>
                <span class="users-list-date"><?= date('d.m.y', $user->created_at) ?></span>
            </li>

        <?php endforeach; ?>

    </ul>
</div>