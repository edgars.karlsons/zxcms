<?php
/**
 * Created by Error202
 * Date: 13.08.2018
 */

use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $notifications array
 * @var $count integer
 */

?>

<li class="dropdown notifications-menu">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-bell-o"></i>
		<?php if ($count > 0): ?>
			<span class="label label-warning"><?= $count ?></span>
		<?php endif; ?>
	</a>
	<ul class="dropdown-menu">
		<li class="header"><?= Yii::t('main', 'You have {count} notifications', ['count' => $count]) ?></li>
		<li>
			<ul class="menu">
				<?php foreach ($notifications as $notification): ?>
					<li>
						<a href="<?= Url::to([$notification['url']]) ?>">
							<i class="fa fa-<?= $notification['icon'] ?> text-<?= $notification['color'] ?>"></i> <?= Yii::t($notification['message_file'], $notification['message'], ['count' => $notification['count']]) ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</li>
		<!-- <li class="footer"><a href="#">View all</a></li> -->
	</ul>
</li>

