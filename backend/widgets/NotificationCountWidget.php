<?php
/**
 * Created by Error202
 * Date: 13.08.2018
 */

namespace backend\widgets;

use Yii;
use yii\base\Widget;

class NotificationCountWidget extends Widget
{
	public function run()
	{
		$count = 0;

		if (isset(Yii::$app->params['notifications'])) {
			foreach ( Yii::$app->params['notifications'] as $notification ) {
				$count += $notification['count'];
			}
		}

		return $this->render('notification-count', [
			'notifications' => Yii::$app->params['notifications'] ?? [],
			'count' => $count,
		]);
	}
}