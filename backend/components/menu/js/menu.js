$(document).ready(function()
{
    $('#nestable3').nestable();

    $('#serialize').click(function(){
        serializeTree();
    });

    //$('#nestable3').on('change', function() {
        /* on change event */
        //alert('Order changed');
    //});

    //$('.panel-group').mouseover(function(ev) {
    //    $(this).find('.dd-item').trigger(ev);
    //});
    //$('.panel-group, .panel, .panel-heading, .panel-title, .panel-collapse, .panel-body').trigger('mouseover');
    //$('.panel-group, .panel, .panel-heading, .panel-title, .panel-collapse, .panel-body, .dd-handle').mouseover();
    //$('.panel-group, .panel, .panel-heading, .panel-title, .panel-collapse, .panel-body, .dd-handle').mouseenter();
});

function serializeTree() {
    //if (window.JSON) {
        //var serialized = JSON.stringify($('#nestable3').nestable('serialize'));
        //$('#output').text(serialized+'\n\n');
        var arr = getPositions($('#nestable3').nestable('serialize'));
        return JSON.stringify(arr);
        //$('#output').text(JSON.stringify(arr));
    //} else {
        //$('#output').text('JSON browser support required for this demo.');
    //}
}

function getPositions(array, parent_id) {
    parent_id = parent_id || 0;
    var new_array = [];
    for (var i = 0; i < array.length; i++) {
        new_array.push([array[i].id, parent_id]);
        if (array[i].hasOwnProperty('children')) {
            new_array = new_array.concat(getPositions( array[i].children, array[i].id ));
        }
    }
    return new_array;
}

function sendTree(menu_id, url, redirect) {
    var json = serializeTree();

    $.ajax({
        url: url,
        type: 'POST',
        data: { json : json },
        success: function(){
            document.location.href = redirect;
        },
        error: function(){
            alert('Error: ' + res.message);
        }
    });
}