<?php
/**
 * Created by Error202
 * Date: 11.07.2018
 */

namespace backend\components\menu\assets;


use yii\web\AssetBundle;

class MenuAsset extends AssetBundle
{
	public $sourcePath = '@backend/components/menu';
	public $css = [
		'css/menu.css',
	];

	public $js = [
		'js/jquery.nestable.js',
		'js/menu.js'
	];

	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapPluginAsset',
	];

	public $publishOptions = [
		'forceCopy' => YII_ENV_DEV ? true : false,
	];
}