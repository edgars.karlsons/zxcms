<?php
/**
 * Created by Error202
 * Date: 24.08.2018
 */

use core\forms\menu\MenuItemForm;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $form ActiveForm
 * @var $model MenuItemForm
 * @var $language string
 */

$postfix = $language == Yii::$app->params['defaultLanguage'] ? '' : '_' . $language;
?>

<div class="row mt-3">
	<div class="col-md-6">
		<?= $form->field($model, 'name' . $postfix)->textInput(['maxlength' => true, 'class' => 'form-control form-control-sm']) ?>
	</div>
	<div class="col-md-6">
		<?= $form->field($model, 'title_attr' . $postfix)->textInput(['maxlength' => true, 'class' => 'form-control form-control-sm']) ?>
	</div>
</div>
