<?php

namespace backend\controllers;

use core\entities\Slider;
use core\forms\SliderForm;
use core\services\SliderService;
use DomainException;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

class SliderController extends Controller
{
    private SliderService $service;

    public function __construct($id, $module, SliderService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'index', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['SliderManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(): string
    {
        $query        = Slider::find();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'       => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     */
    public function actionCreate(): Response|string
    {
        $form           = new SliderForm();
        $form->scenario = Slider::SCENARIO_CREATE;
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $slider = $this->service->create($form);

                return $this->redirect(['view', 'id' => $slider->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id): Response|string
    {
        $slider = $this->findModel($id);

        $form           = new SliderForm($slider);
        $form->scenario = Slider::SCENARIO_UPDATE;
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($slider->id, $form);

                return $this->redirect(['view', 'id' => $slider->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model'  => $form,
            'slider' => $slider,
        ]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionDelete($id): Response
    {
        $this->service->remove($id);

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Slider|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id): ?Slider
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
