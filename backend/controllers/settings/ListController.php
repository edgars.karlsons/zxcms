<?php
/**
 * Created by Error202
 * Date: 04.06.2018
 */

namespace backend\controllers\settings;

use backend\forms\SettingsSearch;
use core\entities\Settings;
use core\forms\FaviconForm;
use core\forms\SettingsForm;
use core\services\SettingsService;
use DomainException;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use backend\components\ToggleAction;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class ListController extends Controller
{
    private SettingsService $service;

    public function __construct(string $id, $module, SettingsService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'index', 'update', 'delete', 'toggle', 'favicon'],
                        'allow'   => true,
                        'roles'   => ['SettingsManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'toggle' => [
                'class'      => ToggleAction::class,
                'modelClass' => Settings::class,
            ]
        ];
    }

    public function actionIndex($section = 'site'): string
    {
        $searchModel  = new SettingsSearch();
        $searchModel->section = $section;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
                'section' => $section,
            ]
        );
    }

    /**
     * @param $section
     * @param $key
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($section, $key): string
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($section, $key),
            ]
        );
    }

    public function actionCreate(): Response|string
    {
        $form = new SettingsForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $settings = $this->service->create($form);

                return $this->redirect(['view', 'section' => $settings->section, 'key' => $settings->key]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        } else {
            $form->active = 1;
        }

        return $this->render(
            'create',
            [
                'model' => $form,
            ]
        );
    }

    /**
     * @param $section
     * @param $key
     * @return Response|string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($section, $key): Response|string
    {
        $settings = $this->findModel($section, $key);

        $form = new SettingsForm($settings);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($settings->section, $settings->key, $form);

                return $this->redirect(['view', 'section' => $settings->section, 'key' => $settings->key]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render(
            'update',
            [
                'model'    => $form,
                'settings' => $settings,
            ]
        );
    }

    public function actionDelete($section, $key): Response
    {
        $this->service->remove($section, $key);

        return $this->redirect(['index']);
    }

    public function actionFavicon(): Response|string
    {
        $form = new FaviconForm();
        if ($form->load(Yii::$app->request->post())) {
            $form->image = UploadedFile::getInstance($form, 'image');
            if ($form->image && $form->validate()) {
                if (!file_exists(Yii::getAlias('@staticRoot') . '/origin/favicon')) {
                    mkdir(Yii::getAlias('@staticRoot') . '/origin/favicon', 0777, true);
                }

                $form->image->saveAs(Yii::getAlias('@staticRoot') . '/origin/favicon/favicon.png');
                $this->service->newFavicon();
                Yii::$app->session->setFlash('success', Yii::t('main', 'Favicon generation complete'));
                return $this->redirect(['settings/list/index']);
            }
        }

        return $this->render(
            'favicon',
            [
                'model'    => $form,
            ]
        );
    }

    /**
     * @param $section
     * @param $key
     * @return array|Settings|ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findModel($section, $key): array|Settings|ActiveRecord|null
    {
        if (($model = Settings::find()->andWhere(['section' => $section])->andWhere(['key' => $key])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested setting does not exist.');
        }
    }
}
