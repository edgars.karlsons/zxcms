<?php

namespace backend\controllers\post;

use core\entities\post\PostType;
use core\forms\post\PostTagSingleForm;
use core\services\post\PostTagManageService;
use DomainException;
use Yii;
use core\entities\post\PostTag;
use core\forms\post\search\PostTagSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

class TagController extends Controller
{
    private PostTagManageService $service;

    public function __construct($id, $module, PostTagManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['PostManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	/**
	 * @param $tid
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionIndex($tid): string
    {
	    $type = $this->findType($tid);

        $searchModel = new PostTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionView($id): string
    {
    	$tag = $this->findModel($id);
	    $type = $this->findType($tag->type_id);
        return $this->render('view', [
            'tag' => $tag,
            'type' => $type,
        ]);
    }

	/**
	 * @param $tid
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException
	 */
    public function actionCreate($tid): Response|string
    {
	    $type = $this->findType($tid);
        $form = new PostTagSingleForm();
	    $form->type_id = $type->id;
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $tag = $this->service->create($form);
                return $this->redirect(['view', 'id' => $tag->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
            'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException
	 */
    public function actionUpdate($id): Response|string
    {
        $tag = $this->findModel($id);
	    $type = $this->findType($tag->type_id);
        $form = new PostTagSingleForm($tag);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($tag->id, $form);
                return $this->redirect(['view', 'id' => $tag->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'tag' => $tag,
	        'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return Response
	 * @throws NotFoundHttpException
	 */
    public function actionDelete($id): Response
    {
	    $tag = $this->findModel($id);
	    $type = $this->findType($tag->type_id);
        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index', 'tid' => $type->id]);
    }

    /**
     * @param integer $id
     * @return PostTag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): PostTag
    {
        if (($model = PostTag::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return PostType
     * @throws NotFoundHttpException
     */
	protected function findType($id): PostType
	{
		if (($type = PostType::findOne($id)) !== null) {
			return $type;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
