<?php

namespace backend\bootstrap;

use backend\widgets\dashboard\helpers\UsersLastInfo;
use core\entities\Settings;
use zertex\elfinder\ElFinder;
use zertex\ckeditor\CKEditor;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = \Yii::$container;

        // init presets
        $presetsPath = \Yii::getAlias('@core/components/ckeditor/presets');
        // basic
        $app->params['ckeditor']['basic']['toolbar'] = require $presetsPath . '/basic/toolbar.php';
        $app->params['ckeditor']['basic']['plugins'] = require $presetsPath . '/basic/plugins.php';
        // editor
        $app->params['ckeditor']['editor']['toolbar'] = require $presetsPath . '/editor/toolbar.php';
        $app->params['ckeditor']['editor']['plugins'] = require $presetsPath . '/editor/plugins.php';
        // debeloper
        $app->params['ckeditor']['developer']['toolbar'] = require $presetsPath . '/developer/toolbar.php';
        $app->params['ckeditor']['developer']['plugins'] = require $presetsPath . '/developer/plugins.php';

        // set preset example
        /*$form->field($model, 'content')->widget(CKEditor::class, [
            'editorOptions' => \zertex\elfinder\ElFinder::ckeditorOptions('elfinder',[
                'toolbar' => Yii::$app->params['ckeditor']['editor']['toolbar'],
                'extraPlugins' => Yii::$app->params['ckeditor']['editor']['plugins'],
            ])
        ])*/

        $container->set(CKEditor::class, [
            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                'toolbar'      => $app->params['ckeditor']['editor']['toolbar'],
                'extraPlugins' => $app->params['ckeditor']['editor']['plugins'],
                'language'     => $app->language,
            ]),
        ]);

        // load settings
        $settings = Settings::find()->with('translations')->andWhere(['active' => 1])->all();
        $settings_array = $settings ? ArrayHelper::map($settings, 'key', function ($el) {
            return $el->translation->value ?? '';
        }, 'section') : [];
        $app->params['settings'] = $settings_array;

        // Connect backend modules

        // Add finish UrlRules
        $app->getUrlManager()->addRules([
            '<_c:[\w\-]+>'                       => '<_c>/index',
            '<_c:[\w\-]+>/<id:\d+>'              => '<_c>/view',
            '<_c:[\w\-]+>/<_a:[\w-]+>'           => '<_c>/<_a>',
            '<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_c>/<_a>',
        ]);

        // Add dashboard widgets
        $app->params['dashboard_widgets'][] = [
                'name' => \Yii::t('user', 'Users'),
                'title' => \Yii::t('user', 'Users'),
                'method' => 'widgetLastUsers',
                'resizable' => 1,
                'size' => [
                    'width' => 4,
                    'height' => 12,
                ],
                'icon' => 'fa fa-user',
                'widget' => UsersLastInfo::class,
                'category' => 'info',
        ];
    }
}
