<?php
/**
 * Created by Error202
 * Date: 15.08.2017
 */

namespace backend\forms\rbac;

use yii\base\Model;
use Yii;

class RbacCreatePermissionForm extends Model
{
    public ?string $name = null;
    public ?string $description = null;
    public ?string $rule_name = null;
    public ?string $data = null;
    public ?int $created_at = null;
    public ?int $updated_at = null;

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name', 'description', 'rule_name'], 'trim'],

            [['description', 'data'], 'string'],
            [['rule_name'], 'string', 'max' => 64],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('user', 'Permission Name'),
            'description' => Yii::t('user', 'Permission Description'),
            'rule_name' => Yii::t('user', 'Rule Name'),
            'data' => Yii::t('user', 'Permission Data'),
        ];
    }
}
