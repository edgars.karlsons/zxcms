<?php

use core\entities\post\PostType;
use kartik\form\ActiveForm;
use core\forms\SearchForm;
use yii\helpers\Html;
use yii\helpers\Url;

$model = new SearchForm();
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->avatar->show(Yii::$app->user->identity->user->username) ?>" class="img-circle"
                     alt="<?= Yii::$app->user->identity->user->username ?>"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->user->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> <?= Yii::t('main', 'Online') ?></a>
            </div>
        </div>

        <!-- search form -->
        <?php $form = ActiveForm::begin([
            'action'                 => Url::to(['/site/search']),
            'options'                => [
                'class' => 'sidebar-form'
            ],
            'enableClientValidation' => false,
            'fieldConfig'            => [
                'options' => [
                    'tag' => false,
                ],
            ],
        ]); ?>

        <?= $form->field($model, 'query', [
            'addon'    => [
                'append' => [
                    'content'  => Html::button('<i class="fa fa-search"></i>', ['class' => 'btn btn-flat', 'id' => 'search-btn']),
                    'asButton' => true
                ]
            ],
            'template' => "{input}",
        ])->textInput(['placeholder' => Yii::t('main', 'Search...')])->label(false)->hint(false); ?>

        <?php ActiveForm::end(); ?>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items'   => \yii\helpers\ArrayHelper::merge([
                    ['label' => Yii::t('main', 'Menu'), 'options' => ['class' => 'header']],

                    [
                        'label'  => Yii::t('dashboard', 'Dashboard'),
                        'icon'   => 'dashboard',
                        'url'    => ['/site/index'],
                        'active' => $this->context->id == '/site/index'
                    ],

                    [
                        'label'   => Yii::t('user', 'Users'),
                        'icon'    => 'user',
                        'url'     => ['/user/index'],
                        'active'  => $this->context->id == 'user',
                        'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('UserManagement'),
                    ],

                    [
                        'label'   => Yii::t('user', 'Access Rules'),
                        'icon'    => 'lock',
                        'items'   => [
                            [
                                'label'  => Yii::t('user', 'Roles'),
                                'icon'   => 'caret-right',
                                'url'    => ['/role/index'],
                                'active' => \Yii::$app->controller->getUniqueId() == 'role'
                            ],
                            [
                                'label'  => Yii::t('user', 'Permissions'),
                                'icon'   => 'caret-right',
                                'url'    => ['/permission/index'],
                                'active' => \Yii::$app->controller->getUniqueId() == 'permission'
                            ],
                        ],
                        'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('UserManagement')
                    ],

                    [
                        'label'   => Yii::t('menu', 'Menu'),
                        'icon'    => 'bars',
                        'url'     => ['/menu/index'],
                        'active'  => $this->context->id == 'menu',
                        'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('MenuManagement'),
                    ],

                    [
                        'label'   => Yii::t('main', 'Modules'),
                        'icon'    => 'cubes',
                        'items'   => [
                            [
                                'label'  => Yii::t('main', 'Modules'),
                                'icon'   => 'caret-right',
                                'url'    => ['/module/list'],
                                'active' => \Yii::$app->controller->action->getUniqueId() == 'module/list'
                            ],
                            [
                                'label'  => Yii::t('main', 'Find modules'),
                                'icon'   => 'caret-right',
                                'url'    => ['/module/search'],
                                'active' => \Yii::$app->controller->action->getUniqueId() == 'module/search'
                            ],
                        ],
                        'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('ModuleManagement')
                    ],

                    /*[
                        'label' => Yii::t('pages', 'Pages'),
                        'icon' => 'file-o', 'url' => ['/page/index'],
                        'active' => $this->context->id == 'page',
                        'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('PagesManagement'),
                    ],*/

                    [
                        'label'   => Yii::t('post', 'Posts'),
                        'icon'    => 'newspaper-o',
                        'items'   => array_merge(
                            array_map(function (PostType $type) {
                                return [
                                    'label' => $type->plural,
                                    'icon'  => 'caret-right',
                                    'items' => [
                                        [
                                            'label'  => Yii::t('post', 'Categories'),
                                            'icon'   => 'caret-right',
                                            'url'    => ['/post/category', 'tid' => $type->id],
                                            'active' => \Yii::$app->controller->getUniqueId() == 'post/category' && \Yii::$app->request->get('tid') == $type->id
                                        ],
                                        [
                                            'label'  => $type->plural,
                                            'icon'   => 'caret-right',
                                            'url'    => ['/post/post', 'tid' => $type->id],
                                            'active' => \Yii::$app->controller->getUniqueId() == 'post/post' && \Yii::$app->request->get('tid') == $type->id
                                        ],
                                        [
                                            'label'  => Yii::t('post', 'Tags'),
                                            'icon'   => 'caret-right',
                                            'url'    => ['/post/tag', 'tid' => $type->id],
                                            'active' => \Yii::$app->controller->getUniqueId() == 'post/tag' && \Yii::$app->request->get('tid') == $type->id
                                        ],
                                    ]
                                ];
                            }, PostType::find()->all()),
                            [
                                [
                                    'label'  => Yii::t('post', 'Comments'),
                                    'icon'   => 'caret-right',
                                    'url'    => ['/post/comment'],
                                    'active' => \Yii::$app->controller->getUniqueId() == 'post/comment'
                                ]
                            ],
                            [
                                [
                                    'label'  => Yii::t('post', 'Types'),
                                    'icon'   => 'caret-right',
                                    'url'    => ['/post/type'],
                                    'active' => \Yii::$app->controller->getUniqueId() == 'post/type'
                                ]
                            ]
                        ),
                        'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('PostManagement')
                    ],

                    [
                        'label'   => Yii::t('slider', 'Slider'),
                        'icon'    => 'retweet',
                        'url'     => ['/slider/index'],
                        'active'  => $this->context->id == 'slider',
                        'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('SliderManagement'),
                    ],

                    [
                        'label'   => Yii::t('main', 'Settings'),
                        'icon'    => 'cog',
                        'items'   => [
                            [
                                'label'  => Yii::t('main', 'Settings List'),
                                'icon'   => 'caret-right',
                                'url'    => ['/settings/list/index'],
                                'active' => \Yii::$app->controller->getUniqueId() == 'settings/list'
                            ],
                            //['label' => Yii::t('main', 'Find modules'), 'icon' => 'caret-right', 'url' => ['/module/search'], 'active' => \Yii::$app->controller->action->getUniqueId() == 'module/search'],
                        ],
                        'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('SettingsManagement')
                    ],

                ], (isset(Yii::$app->params['adminMenu']) && is_array(Yii::$app->params['adminMenu']) ? Yii::$app->params['adminMenu'] : [])),
            ]
        ) ?>

    </section>

</aside>
