<?php

use core\widgets\FaviconWidget;
use core\components\adminlte3\themes\AdminLteLightAsset;
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\web\View;
use core\components\adminlte3\AdminLteAsset;
use core\components\adminlte3\AdminLteFontsAsset;

/* @var $this View */
/* @var $content string */

if (Yii::$app->controller->id == 'auth/auth' && in_array(Yii::$app->controller->action->id, ['login', 'request', 'confirm', 'login-hub', 'login-token', 'reset'])) {
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    AppAsset::register($this);
    AdminLteAsset::register($this);
    AdminLteFontsAsset::register($this);

//if (Yii::$app->params['theme'] == \app\forms\SettingsForm::THEME_BLACK) {
    //AdminLteDarkAsset::register($this);
//}
//else {
    AdminLteLightAsset::register($this);
//}

    $css = <<<CSS
	.sidebar .nav .nav-link {
		font-weight: 500 !important;
		font-family: "Segoe UI", "Source Sans Pro", -apple-system, BlinkMacSystemFont, Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"
	}
	
	.content-wrapper {
		font-size: 0.9rem !important;
		font-weight: 400 !important;
		font-family: "Segoe UI", "Source Sans Pro", -apple-system, BlinkMacSystemFont, Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"
	}

	.kv-grid-table.table thead th,
	.kv-grid-table.table-bordered thead th,
	.kv-grid-table.table-bordered thead td {
		border-bottom: 0 !important;
	}

	label:not(.form-check-label):not(.custom-file-label) {
		font-weight: 600;
	}
	
	.kv-grid-bs4 a.desc:after {
		content: '\\f160';
		margin-left: 3px;
	}
	
	.kv-grid-bs4 a.asc:after {
		content: '\\f884';
		margin-left: 3px;
	}

	/*.kv-grid-bs4 a:before {
        @extend .fas;
        margin-right: 5px;
    }

    .kv-grid-bs4 a.desc {
        @extend .fa-sort-amount-down;
    }
    .kv-grid-bs4 a.asc {
        @extend .fa-sort-amount-down-alt;
    }*/

	.breadcrumb {
	    font-size: 12px !important;
	}
    .sp-preview {
        margin-right: 0 !important;
    }
    .sp-dd {
        display: none;
    }
CSS;
    $this->registerCss($css);
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <?= FaviconWidget::widget() ?>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <!-- Font Awesome Icons
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css"> -->
        <!-- IonIcons -->
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=swap&subset=cyrillic" rel="stylesheet">
        <?php $this->head() ?>
    </head>

    <body class="hold-transition sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <!-- Navbar -->
        <?= $this->render('header.php') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?= $this->render('left.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <?= $this->render(
            'content.php',
            ['content' => $content]
        ) ?>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <strong>ZxCMS &copy; <?= date('Y') ?>
                <!--<div class="float-right d-none d-sm-inline-block">
			<b><?= Yii::t('main', 'Version') ?></b> 3.0
		</div> -->
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- OPTIONAL SCRIPTS -->
    <!-- <script src="plugins/chart.js/Chart.min.js"></script> -->
    <!-- <script src="dist/js/demo.js"></script>
    <script src="dist/js/pages/dashboard3.js"></script> -->

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>

<?php } ?>

