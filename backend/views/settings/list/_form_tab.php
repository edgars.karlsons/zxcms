<?php
/**
 * Created by Error202
 * Date: 24.08.2018
 */

use core\forms\SettingsForm;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $form ActiveForm
 * @var $model SettingsForm
 * @var $language string
 */

$postfix = $language == Yii::$app->params['defaultLanguage'] ? '' : '_' . $language;

echo $form->field($model, 'value' . $postfix)->textarea(['rows' => 6]);
