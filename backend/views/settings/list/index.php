<?php

use backend\forms\SettingsSearch;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use core\entities\Settings;
use backend\components\ToggleColumn;

/**
 * @var yii\web\View $this
 * @var SettingsSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var string $section
 */

$this->title                   = Yii::t('main', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">

    <p>
        <?=
        Html::a(
            Yii::t('buttons', 'Create Setting'),
            ['create'],
            ['class' => 'btn btn-success']
        ) ?>

        <?=
        Html::a(
            Yii::t('main', 'Change Favicon'),
            ['favicon'],
            ['class' => 'btn btn-default']
        ) ?>
    </p>

    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <?php foreach (Settings::find()->select('section')->distinct()->where(['<>', 'section', ''])->all() as $setting) : ?>
                    <?= Html::a(Yii::t('main', $setting->section), ['/settings/list/index', 'section' => $setting->section], [
                        'class' => 'list-group-item ' . ($section == $setting->section ? 'active' : ''),
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="col-md-9">
            <div class="box">
                <div class="box-body">

                    <?php Pjax::begin(); ?>
                    <?=
                    GridView::widget(
                        [
                            'dataProvider' => $dataProvider,
                            'filterModel'  => $searchModel,
                            'columns'      => [
                                [
                                    'attribute' => 'key',
                                    'options'   => ['style' => 'width: 25%;'],
                                ],
                                [
                                    'label' => Yii::t('main', 'Value'),
                                    'value' => function (Settings $setting) {
                                        return $setting->translation->value ?? '- ';
                                    },
                                    'format' => 'ntext',
                                ],
                                [
                                    'class'          => ToggleColumn::class,
                                    'attribute'      => 'active',
                                    'filter'         => [1 => Yii::t('yii', 'Yes'), 0 => Yii::t('yii', 'No')],
                                    'options'        => ['style' => 'width: 100px;'],
                                    'contentOptions' => ['class' => 'text-center'],
                                ],
                                [
                                    'class'          => 'yii\grid\ActionColumn',
                                    'options'        => ['style' => 'width: 100px;'],
                                    'contentOptions' => ['class' => 'text-center'],
                                ],
                            ],
                        ]
                    ); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>