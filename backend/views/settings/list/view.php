<?php

use core\entities\Settings;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var Settings $model
 */

$this->title                   = $model->section . '.' . $model->key;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="setting-view">

    <p>
        <?= Html::a(Yii::t('buttons', 'All Settings'), ['index', 'section' => $model->section], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'section' => $model->section, 'key' => $model->key], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(
            Yii::t('buttons', 'Delete'),
            ['delete', 'section' => $model->section, 'key' => $model->key],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?=
            DetailView::widget(
                [
                    'model'      => $model,
                    'attributes' => [
                        'type',
                        'section',
                        'active:boolean',
                        'key',
                        [
                            'attribute' => 'created_at',
                            'format'    => ['datetime', 'php:d.m.Y H:i'],
                        ],
                        [
                            'attribute' => 'updated_at',
                            'format'    => ['datetime', 'php:d.m.Y H:i'],
                        ],
                    ],
                ]
            ) ?>

            <?php
            $items = [];
            foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                $items[] = [
                    'label'   => $language_name,
                    'content' => $this->render('_view_tab', [
                        'setting'  => $model,
                        'language' => $language,
                    ]),
                ];
            }
            ?>

            <div class="nav-tabs-custom">
                <?= Tabs::widget([
                    'items' => $items,
                ]) ?>
            </div>
        </div>
    </div>
</div>