<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use core\forms\SettingsForm;

/**
 * @var yii\web\View $this
 * @var SettingsForm $model
 * @var yii\widgets\ActiveForm $form
 */

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="setting-form">

    <div class="box box-default">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'section')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'key')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'active')->checkbox(['value' => 1]) ?>

            <?=
            $form->field($model, 'type')->dropDownList(
                $model->getTypes()
            )->hint(Yii::t('main', 'Change at your own risk')) ?>

            <?php
            $items = [];
            foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                $items[] = [
                    'label'   => $language_name,
                    'content' => $this->render('_form_tab', [
                        'form'     => $form,
                        'model'    => $model,
                        'language' => $language,
                    ]),
                ];
            }
            ?>

            <div class="nav-tabs-custom">
                <?= Tabs::widget([
                    'items' => $items
                ]) ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
