<?php

use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('user', 'Roles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-rbac-role-index">
    <div class="card">
        <div class="card-header">
            <?= Html::a(Yii::t('user', 'Create Role'), ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('user', 'Role Name'),
                    ],
                    [
                        'attribute' => 'description',
                        'label' => Yii::t('user', 'Role Description'),
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
