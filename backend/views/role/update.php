<?php

use backend\forms\rbac\RbacEditRoleForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model RbacEditRoleForm*/

$this->title = Yii::t('user', 'Update Role: {role}', ['role' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="users-rbac-role-update">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-body">
            <?= $form->field($model, 'name')->textInput(['maxLength' => true]) ?>
            <?= $form->field($model, 'description')->textarea() ?>
            <?= $form->field($model, 'rule_name')->textInput(['maxLength' => true]) ?>
            <?= $form->field($model, 'data')->textarea() ?>
        </div>
        <card class="card-footer text-right">
            <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
        </card>
    </div>

    <?php ActiveForm::end(); ?>

</div>
