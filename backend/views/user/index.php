<?php

use backend\widgets\grid\RoleColumn;
use kartik\date\DatePicker;
use core\entities\user\User;
use core\helpers\UserHelper;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="card">
        <div class="card-header">
            <?= Html::a(Yii::t('user', 'Create User'), ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    [
                        'attribute' => 'username',
                        'value' => function (User $model) {
                            return Html::a(Html::encode($model->username), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    'email:email',
	                [
		                'attribute' => 'created_at',
		                'filter' => DatePicker::widget([
			                'model' => $searchModel,
			                'attribute' => 'date_from',
			                'attribute2' => 'date_to',
			                'type' => DatePicker::TYPE_RANGE,
			                'separator' => '-',
			                'pluginOptions' => [
				                'todayHighlight' => true,
				                'autoclose'=>true,
				                'format' => 'yyyy-mm-dd',
			                ],
		                ]),
		                'format' => ['datetime', 'php:d.m.Y H:i'],
		                'options' => ['style' => 'width: 250px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                    [
                        'attribute' => 'role',
                        'label' => Yii::t('user', 'Role'),
                        'class' => RoleColumn::class,
                        'filter' => $searchModel->rolesList(),
                        'options' => ['style' => 'width: 130px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    [
                        'attribute' => 'status',
                        'filter' => UserHelper::statusList(),
                        'value' => function (User $model) {
                            return UserHelper::statusLabel($model->status);
                        },
                        'format' => 'raw',
                        'options' => ['style' => 'width: 130px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
