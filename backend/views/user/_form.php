<?php

use core\entities\user\User;
use core\forms\user\UserForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model UserForm */
/* @var $user User */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>
	<div class="card">
		<div class="card-body">
			<?= $form->field($model, 'username')->textInput(['maxLength' => true]) ?>
			<?= $form->field($model, 'email')->textInput(['maxLength' => true]) ?>
			<?= $form->field($model, 'password')->passwordInput(['maxLength' => true]) ?>
			<?= $form->field($model, 'role')->dropDownList($model->rolesList()) ?>
		</div>
        <div class="card-footer text-right">
            <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
	</div>
	<?php ActiveForm::end(); ?>
</div>
