<?php

use core\entities\user\User;
use core\forms\user\ProfileEditForm;
use kartik\widgets\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model ProfileEditForm */
/* @var $user User */

$this->title = Yii::t('user', 'Profile: {username}', ['username' => $user->username]);
$this->params['breadcrumbs'][] = ['label' => $user->username, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = Yii::t('user', 'Profile');
?>
<div class="user-profile">

	<?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<div style="text-align: center">
						<img style="width: 300px" src="<?= Yii::$app->avatar->show(Yii::$app->user->identity->user->username, null, null, Yii::$app->user->identity->user->user_pic) ?>?<?= rand(10, 5000) ?>" class="img-circle" alt="<?= Yii::$app->user->identity->user->username ?>"/>
					</div>
					<!-- < ?= $form->field($model, 'user_pic')->fileInput() ?> -->
					<?= $form->field($model, 'user_pic')->widget(FileInput::class, [
						'options' => [
							'accept' => 'image/*'
						],
						'pluginOptions' => [
							'showPreview' => false,
							'showCaption' => true,
							'showRemove' => true,
							'showUpload' => false,
						],
					]); ?>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<?= $form->field($model, 'username')->textInput(['maxLength' => true]) ?>
					<?= $form->field($model, 'email')->textInput(['maxLength' => true]) ?>
					<?= $form->field($model, 'password')->passwordInput(['maxLength' => true]) ?>
					<?= $form->field($model, 'backend_language')->dropDownList(Yii::$app->params['backendTranslatedLanguages'], [
						'value' => Yii::$app->user->identity->user->backend_language ?: Yii::$app->params['backendDefaultLanguage'],
					]) ?>
				</div>
			</div>
		</div>
	</div>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
