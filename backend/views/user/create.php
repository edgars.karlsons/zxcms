<?php
use core\forms\user\UserForm;

/* @var $this yii\web\View */
/* @var $model UserForm */

$this->title = Yii::t('user', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
