<?php

use core\entities\post\Post;
use core\entities\post\PostType;
use core\helpers\PostHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $post Post */
/* @var $modificationsProvider yii\data\ActiveDataProvider */
/* @var $type PostType */

$title = $post->title;
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Gallery'), 'url' => ['index', 'tid' => $type->id]];
$this->params['breadcrumbs'][] = $title;
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('post', 'All Posts'), ['index', 'tid' => $type->id], ['class' => 'btn btn-default']) ?>
        <?php if ($post->isActive()): ?>
            <?= Html::a(Yii::t('post', 'Draft'), ['draft', 'id' => $post->id], ['class' => 'btn btn-primary', 'data-method' => 'post']) ?>
        <?php else: ?>
            <?= Html::a(Yii::t('post', 'Activate'), ['activate', 'id' => $post->id], ['class' => 'btn btn-success', 'data-method' => 'post']) ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $post->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $post->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-header with-border"><?= Yii::t('post', 'Common') ?></div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $post,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'status',
                        'value' => PostHelper::statusLabel($post->status),
                        'format' => 'raw',
                    ],
                    'title',
                    [
                        'attribute' => 'category_id',
                        'value' => ArrayHelper::getValue($post, 'category.name'),
                    ],
                    [
                        'label' => Yii::t('post', 'Tags'),
                        'value' => implode(', ', ArrayHelper::getColumn($post->tags, 'name')),
                    ],
                ],
            ]) ?>
        </div>
    </div>

	<?php if ($post->image): ?>
    <div class="box">
        <div class="box-header with-border"><?= Yii::t('post', 'Image') ?></div>
        <div class="box-body">
            <?= Html::img($post->getThumbFileUrl('image', 'thumb_gallery_view'), [
                'class' => 'thumbnail',
	            'width' => 300,
            ]) ?>
        </div>
    </div>
	<?php endif; ?>

	<?php if (!$post->image && $post->video): ?>
		<div class="box">
			<div class="box-header with-border"><?= Yii::t('post', 'Image') ?></div>
			<div class="box-body">
				<?= Html::img('https://i.ytimg.com/vi/' . PostHelper::parseYoutubeUrl($post->video) . '/maxresdefault.jpg', [
					'width' => 300,
					'class' => 'thumbnail',
				]) ?>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($post->video): ?>
    <div class="box">
        <div class="box-header with-border"><?= Yii::t('post', 'Video') ?></div>
        <div class="box-body">
	        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= PostHelper::parseYoutubeUrl($post->video) ?>?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
	<?php endif; ?>

	<div class="box">
		<div class="box-header with-border"><?= Yii::t('post', 'Description') ?></div>
		<div class="box-body">
			<?= Yii::$app->formatter->asNtext($post->description) ?>
		</div>
	</div>

    <div class="box">
        <div class="box-header with-border"><?= Yii::t('post', 'SEO') ?></div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $post,
                'attributes' => [
                    [
                        'attribute' => 'meta.title',
                        'value' => $post->meta->title,
                    ],
                    [
                        'attribute' => 'meta.description',
                        'value' => $post->meta->description,
                    ],
                    [
                        'attribute' => 'meta.keywords',
                        'value' => $post->meta->keywords,
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
