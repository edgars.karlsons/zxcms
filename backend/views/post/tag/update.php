<?php

use core\entities\post\PostTag;
use core\entities\post\PostType;
use core\forms\post\PostTagSingleForm;

/* @var $this yii\web\View */
/* @var $tag PostTag */
/* @var $model PostTagSingleForm */
/* @var $type PostType */

$title = Yii::t('post', 'Update Tag: {name}', ['name' => $tag->name]);
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $tag->name, 'url' => ['view', 'id' => $tag->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
