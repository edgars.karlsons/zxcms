<?php

/* @var $this yii\web\View */
/* @var $model PostTypeForm */
/* @var $type PostType */

use core\entities\post\PostType;
use core\forms\post\PostTypeForm;

$this->title = Yii::t('post', 'Update type: {name}', ['name' => $type->plural]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $type->plural, 'url' => ['view', 'id' => $type->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
