<?php

use core\forms\post\PostTypeForm;

/* @var $this yii\web\View */
/* @var $model PostTypeForm */

$this->title = Yii::t('post', 'Create Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
