<?php

use core\entities\post\PostCategory;
use core\entities\post\PostType;
use core\forms\post\PostCategoryForm;

/* @var $this yii\web\View */
/* @var $category PostCategory */
/* @var $model PostCategoryForm */
/* @var $type PostType */

$title = Yii::t('post', 'Update: {name}', ['name' => $category->name]);
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
