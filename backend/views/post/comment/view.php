<?php

use core\entities\post\Post;
use core\entities\post\PostComment;
use yii\helpers\Html;
use yii\widgets\DetailView;
use core\entities\user\User;

/* @var $this yii\web\View */
/* @var $post Post */
/* @var $comment PostComment */
/* @var $modificationsProvider yii\data\ActiveDataProvider */

$this->title = $post->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('post', 'Comments'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'post_id' => $post->id, 'id' => $comment->id], ['class' => 'btn btn-primary']) ?>
        <?php if ($comment->isActive()): ?>
            <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'post_id' => $post->id, 'id' => $comment->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php else: ?>
            <?= Html::a(Yii::t('buttons', 'Restore'), ['activate', 'post_id' => $post->id, 'id' => $comment->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => Yii::t('buttons', 'Are you sure you want to activate this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $comment,
                'attributes' => [
                    'id',
                    'created_at:boolean',
                    'active:boolean',
                    [
                        'attribute' => 'user_id',
                        'value' => function(PostComment $comment) {
                            return User::findOne($comment->user_id)->username;
                        },
                    ],
                    'parent_id',
                    [
                        'attribute' => 'post_id',
                        'value' => $post->title,
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <div class="box">
        <div class="box-body">
            <?= Yii::$app->formatter->asNtext($comment->text) ?>
        </div>
    </div>

</div>
