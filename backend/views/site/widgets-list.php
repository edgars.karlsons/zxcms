<?php

use yii\helpers\Html;
use backend\helpers\DashboardHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $wl array
 */

list($widgetsCounter, $widgetsInfo) = DashboardHelper::getAllWidgets();
?>

<div class="row">
    <div class="col-md-6">
        <h4><?= Yii::t('main', 'Counters widgets') ?></h4>

        <div style="margin-bottom:15px;">
            <?= Html::dropDownList('colorCounter', 'blue', DashboardHelper::getExtColors(), [
                'id' => 'colorCounter',
                'class' => 'form-control',
            ]) ?>
        </div>
        <?= $widgetsCounter ?>
    </div>
    <div class="col-md-6">
        <h4><?= Yii::t('main', 'Info widgets') ?></h4>
        <div style="margin-bottom:15px;">
            <?= Html::dropDownList('colorInfo', 'primary', DashboardHelper::getColors(), [
                'id' => 'colorInfo',
                'class' => 'form-control',
            ]) ?>
        </div>
        <?= $widgetsInfo ?>
    </div>
</div>

