<?php

use core\helpers\UserHelper;
use zertex\gridstack\Gridstack;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::t('dashboard', 'Dashboard');

$css = '
        .grid-stack {
            margin: 0 !important;
            padding: 0 !important;
            /*border: solid 1px red;*/
        }
        .grid-stack-item {
            margin: 0 !important;
            padding: 0 !important;
            /*border: solid 1px green;*/
            overflow: hidden;
        }
        .grid-stack-item-content {
            margin: 0 !important;
            padding: 0 !important;
            overflow: hidden !important;
            left: 0 !important;
            right: 15px !important;
            /*bottom: 40px !important;*/
            /*bottom: auto !important;*/
        }
        
        .moveButton {
            font-size:12px;
            color:#999;
            cursor: pointer;
            margin-right:5px;
            display: inline-block;
        }
        .moveButton:hover {
            color:#333;
        }

        .user128 {
            width: 128px;
            height: 128px;
        }
        
        .big_drop .select2-results ul {
            max-height: 500px !important;
            min-height: 500px !important;
            height: 500px !important;
        }  
';
$this->registerCss($css);

$addWidgetUrl = Url::toRoute('/site/add-widget');
$removeWidgetUrl = Url::toRoute('/site/remove-widget');
$js = <<<JS
    var AddWidgetDialog;
    var RemoveWidgetDialog;

    function addWidget(itemIdx, colorId)
    {
        var color = $("#"+colorId).val();
        $.ajax({
            method: "GET",
            data: {itemIdx: itemIdx, color: color},
            url: "{$addWidgetUrl}"
        })
        .done(function(data){
            AddWidgetDialog.close();
            document.location.reload();
        });
        return false;
    }

    function removeWidget(idx)
    {
        $.ajax({
            method: "GET",
            data: {idx: idx},
            url: "{$removeWidgetUrl}"
        })
        .done(function(data){
            RemoveWidgetDialog.close();
            document.location.reload();
        });
        return false;
    }
JS;

$this->registerJs($js, $this::POS_HEAD);

$saveWidgetsUrl = Url::toRoute('/site/save-widgets');
$js2 = '
    $("#addWidgetButton").on("click", function() {
        krajeeDialog.dialog(
            "",
            function (result) {alert(result);}
        );
    });
    
    $("#removeWidgetButton").on("click", function() {
        krajeeDialogRemove.dialog(
            "",
            function (result) {alert(result);}
        );
    });

    $(".grid-stack").on("change", function(event, items) {
        var widgets = [];

        $(".grid-stack-item.ui-draggable").each(function () {
            var $this = $(this);
            widgets.push({
                x: $this.attr("data-gs-x"),
                y: $this.attr("data-gs-y"),
                w: $this.attr("data-gs-width"),
                h: $this.attr("data-gs-height"),
                c: $this.attr("data-color"),
                title: $this.attr("data-title"),
                name: $this.attr("data-name"),
                method: $this.attr("data-method"),
                resize: $this.attr("data-resize"),
                widget: $this.attr("data-widget"),
                icon: $this.attr("data-icon"),
            });
        });
        var widgetsJson = JSON.stringify(widgets);
        $.ajax({
            method: "POST",
            data: {widgets: widgetsJson},
            url: "' . $saveWidgetsUrl . '",
        })
        .done(function(data){
        });
    });
';

$this->registerJs($js2, $this::POS_READY);

$formatJs = <<<JS
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<b style="margin-left:5px">' + repo.text + '</b>' + 
    '</div>' +
'</div>';
    if (repo.description) {
      markup += '<h5>' + repo.description + '</h5>';
    }
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.full_name || repo.text;
}
JS;
$this->registerJs($formatJs, $this::POS_HEAD);
?>

<div class="site-index">

    <?= Dialog::widget([
        'options' => [
            'title' => Yii::t('main', 'Add widget'),
            'buttons' => [
                [
                    'id' => 'cancel-1',
                    'label' => Yii::t('main', 'Cancel'),
                    'action' => new JsExpression('function(dialog) {
                        dialog.close();
                    }')
                ],
            ],
            'onshown' => new JsExpression("function(dialog){
                var url = '" . Url::toRoute('/site/get-widgets-list') . "';
                $.ajax({
                    method: 'POST',
                    url: url,
                })
                .done(function(data){
                    dialog.getModalBody().html(data);
                    AddWidgetDialog = dialog;
                });
            }"),
        ],
    ]); ?>

    <?= Dialog::widget([
        'libName' => 'krajeeDialogRemove',
        'options' => [
            'title' => Yii::t('main', 'Remove widget'),
            'type' => Dialog::TYPE_DANGER,
            'buttons' => [
                [
                    'id' => 'cancel-1',
                    'label' => Yii::t('main', 'Cancel'),
                    'action' => new JsExpression('function(dialog) {
                        dialog.close();
                    }')
                ],
            ],
            'onshown' => new JsExpression("function(dialog){
                var url = '" . Url::toRoute('/site/get-selected-widgets-list') . "';
                $.ajax({
                    method: 'POST',
                    url: url,
                })
                .done(function(data){
                    dialog.getModalBody().html(data);
                    RemoveWidgetDialog = dialog;
                });
            }"),
        ],
    ]); ?>

    <?php
    $gridStack = Gridstack::begin([
        'options'       => ['class' => 'grid-stack'],
        'clientOptions' => [
            'cellHeight'     => 22,
            'verticalMargin' => 15,
            'handle'         => '.moveHandle',
        ],
    ]);
    $widgets = UserHelper::getSetting('widgetsLayout', []);
    foreach ($widgets as $widget) {
        $widgetData = [
            'class'=>'grid-stack-item',
            'data-gs-width'=>$widget['w'],
            'data-gs-height'=>$widget['h'],
            'data-gs-x'=>$widget['x'],
            'data-gs-y'=>$widget['y'],
            'data-method' => $widget['method'],
            'data-title' => $widget['title'],
            'data-color' => $widget['c'],
            'data-resize' => $widget['resize'],
            'data-widget' => $widget['widget'],
            'data-icon' => $widget['icon'],
            'data-name' => $widget['name'],
        ];
        // Disable resize if need
        if ($widget['resize'] == 0) {
            $widgetData['data-gs-no-resize'] = 'y';
        }

        if (isset($widget['auto']) && $widget['auto'] == 1) {
            $widgetData['data-gs-auto-position'] = 'y';
        }

        // Remove (), if exists
        $widget['method'] = str_replace('()', '', $widget['method']);
        // Echo widget data
        echo $gridStack->beginWidget($widgetData);
        echo call_user_func($widget['widget'] . '::' . $widget['method'], $widget['c']);
        echo $gridStack->endWidget();
    }
    ?>

    <?php Gridstack::end(); ?>

    <div class="clearfix"></div>
    <hr>

    <div class="row" style="text-align: right; padding-right:15px;">
        <button class="btn btn-default" id="addWidgetButton"><i class="fa fa-plus"></i></button>
        <button class="btn btn-default" id="removeWidgetButton"><i class="fa fa-minus"></i></button>
    </div>

</div>
