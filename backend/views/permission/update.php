<?php

use backend\forms\rbac\RbacEditPermissionForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model RbacEditPermissionForm */

$this->title = Yii::t('user', 'Update Permission: {permission}', ['permission' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('user', 'Editing');
?>
<div class="users-rbac-permission-update">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-body">
            <?= $form->field($model, 'name')->textInput(['maxLength' => true]) ?>
            <?= $form->field($model, 'description')->textarea() ?>
            <?= $form->field($model, 'rule_name')->textInput(['maxLength' => true]) ?>
            <?= $form->field($model, 'data')->textarea() ?>
        </div>
        <card class="card-footer text-right">
            <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
        </card>
    </div>

    <?php ActiveForm::end(); ?>

</div>