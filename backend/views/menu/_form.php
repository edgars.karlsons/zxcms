<?php

use core\components\bootstrap4\widgets\Tab4;
use core\forms\menu\MenuForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model MenuForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-body">

	        <?php
	        $items = [];
	        foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
		        $items[] = [
			        'label' => $language_name,
			        'content' => $this->render('_form_tab', [
			        	'form' => $form,
				        'model' => $model,
				        'language' => $language,
			        ]),
		        ];
	        }
	        ?>

	        <div class="nav-tabs-custom">
		        <?= Tab4::widget([
					'items' => $items
		        ]) ?>
	        </div>

        </div>
        <div class="card-footer text-right">
            <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
