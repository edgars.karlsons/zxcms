<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

use backend\components\menu\widgets\MenuEditorWidget;
use core\entities\menu\Menu;
use core\forms\menu\MenuSelectForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model MenuSelectForm
 * @var $menus Menu[]
 * @var $menu Menu
 * @var $creator array
 */

$this->title                   = Yii::t('menu', 'Menu');
$this->params['breadcrumbs'][] = Html::encode(isset($menu->translation) ? $menu->translation->name : Yii::t('menu', 'Not set'));
$this->params['breadcrumbs'][] = $this->title;

$js2 = <<<JS2
    $("#copyEditorCodeButton").on('click', function(){
        CopyToClipboard($('#code_editor'));
    });
    
    $("#copyTemplateCodeButton").on('click', function(){
        CopyToClipboard($('#code_template'));
    });
JS2;
$this->registerJs($js2, $this::POS_READY);

$js = <<<JS
    function CopyToClipboard(selector) {
        var body = $("body");
        selector.select();
        document.execCommand("copy");
    }
JS;
$this->registerJs($js, $this::POS_HEAD);
?>

<div class="menu">
    <div class="card">
        <div class="card-header"><?= Yii::t('menu', 'Current Menu') ?></div>
        <div class="card-body">

            <div class="row menu-selector">
                <div class="col-md-6">
                    <?php $form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'method'                 => 'get',
                        'id'                     => 'select_menu',
                    ]); ?>

                    <?= $form->field($model, 'id')->dropDownList($menus, [
                        'class' => 'form-control mb-1',
                        //'prompt' => Yii::t('menu', 'Select menu...'),
                        'value'    => $menu->id,
                        'onchange' => 'this.form.submit()',
                    ])->label(false) ?>

                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-md-6">
                    <?= Html::a(Yii::t('menu', 'Create Menu'), ['menu/create'], [
                        'class' => 'btn btn-success d-block d-md-inline-block mb-1',
                    ]) ?>

                    <?= Html::a(Yii::t('menu', 'Update Menu'), ['menu/update', 'id' => $menu->id], [
                        'class' => 'btn btn-primary d-block d-md-inline-block mb-1',
                    ]) ?>

                    <?= Html::a(Yii::t('menu', 'Delete Menu'), ['menu/delete', 'id' => $menu->id], [
                        'class' => 'btn btn-danger d-block d-md-inline-block mb-1',
                        'data'  => [
                            'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                            'method'  => 'post',
                        ],
                    ]) ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header"><?= Yii::t('menu', 'Available Blocks') ?></div>
                <div class="card-body">

                    <div id="accordion">

                        <?php foreach ($creator as $item) : ?>

                            <div class="card">
                                <div class="card-header" style="padding-left: 0; border: 0 !important;">
                                    <h4 class="card-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse<?= $item['id'] ?>"><?= $item['title'] ?>
                                            <i class="fa fa-angle-down" style="position: absolute; right: 10px; top 10px;" aria-hidden="true"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?= $item['id'] ?>" class="panel-collapse collapse">
                                    <div class="card-body">
                                        <?= $item['content'] ?>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header"><?= Yii::t('menu', 'Menu Items') ?></div>
                <div class="card-body">

                    <?= MenuEditorWidget::widget([
                        'menu_id' => $menu->id
                    ]) ?>

                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-header"><?= Yii::t('menu', 'Insert Code') ?></div>
                <div class="card-body">

                    <?php
                    $code = "<?= \core\widgets\menu\MenuWidget::widget(['menu_id' => " . $menu->id . ']) ?>';
                    ?>

                    <label for="code_template"><?= Yii::t('menu', 'For template') ?></label>
                    <div class="input-group">
                        <?= Html::input('text', 'code_template', $code, [
                            'class' => 'form-control form-control-sm',
                            'id' => 'code_template',
                        ]) ?>
                        <span class="input-group-btn">
                      <button id="copyTemplateCodeButton" type="button" class="btn btn-default btn-flat btn-sm"><i class="fas fa-copy" aria-hidden="true"></i></button>
                    </span>
                    </div>

                    <?php
                    $code = "[?= \core\widgets\menu\MenuWidget::widget(['menu_id' => " . $menu->id . ']) ?]';
                    ?>

                    <label for="code_template" class="mt-2"><?= Yii::t('menu', 'For editor') ?></label>
                    <div class="input-group">
                        <?= Html::input('text', 'code_editor', $code, [
                            'class' => 'form-control form-control-sm',
                            'id' => 'code_editor',
                        ]) ?>
                        <span class="input-group-btn">
                      <button id="copyEditorCodeButton" type="button" class="btn btn-default btn-flat btn-sm"><i class="fas fa-copy" aria-hidden="true"></i></button>
                    </span>
                    </div>

                </div>
            </div>
        </div>


    </div>

