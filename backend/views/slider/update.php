<?php

use core\entities\Slider;
use core\forms\SliderForm;

/* @var $this yii\web\View */
/* @var $model SliderForm */
/* @var $slider Slider */

$this->title = Yii::t('slider', 'Update Slide');
$this->params['breadcrumbs'][] = ['label' => Yii::t('slider', 'Slider'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $slider->title, 'url' => ['view', 'id' => $slider->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="slider-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
