<?php

use core\forms\SliderForm;
use kartik\widgets\FileInput;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model SliderForm */
?>

<div class="slider-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="card">
		<div class="card-body">
			<?= $form->field($model, 'title')->textarea(['maxLength' => true, 'rows' => 2]) ?>
			<?= $form->field($model, 'tagline')->textarea(['maxLength' => true, 'rows' => 2]) ?>

			<div class="row">
				<div class="col-md-2">
					<?= $form->field($model, 'sort')->textInput(['maxLength' => true]) ?>
				</div>
				<div class="col-md-10">
					<?= $form->field($model, 'url')->textInput(['maxLength' => true]) ?>
				</div>
			</div>

			<?=  $form->field($model, 'image')->widget(FileInput::class, [
				'pluginOptions' => [
					'showUpload' => false,
				],
			])->label(Yii::t('slider', 'Background Image')) ?>
		</div>
        <div class="card-footer text-right">
            <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
	</div>

	<?php ActiveForm::end(); ?>

</div>

