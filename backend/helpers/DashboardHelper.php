<?php
/**
 * Created by Error202
 * Date: 19.12.2017
 */

namespace backend\helpers;

use Yii;
use yii\helpers\Html;

class DashboardHelper
{
    public static function getAllWidgets(): array
    {
        $widgetsCounter = ''; // Counters Widgets
        $widgetsInfo    = ''; // Information Widgets

        // Get all items in widgets/items folder
        $path  = Yii::getAlias('@backend/widgets/dashboard');
        $files = scandir($path, 0);

        foreach (Yii::$app->params['dashboard_widgets'] as $idx => $item) {
            if (isset($item['rule']) && (Yii::$app->user->can($item['rule']) || Yii::$app->user->can('admin'))) {
            } elseif (!isset($item['rule'])) {
            } else {
                continue;
            }

            if ($item['category'] == 'counter') {
                $line           = Html::a($item['name'], '#', [
                        'onclick' => 'addWidget(' . $idx . ", 'colorCounter')",
                    ]) . '<br>';
                $widgetsCounter .= $line;
            } else {
                $line        = Html::a($item['name'], '#', [
                        'onclick' => 'addWidget(' . $idx . ", 'colorInfo')",
                    ]) . '<br>';
                $widgetsInfo .= $line;
            }
        }
        return [$widgetsCounter, $widgetsInfo];
    }

    public static function getExtColors(): array
    {
        return [
            'gray'       => Yii::t('main', 'Gray'),
            'gray-light' => Yii::t('main', 'Light gray'),
            'black'      => Yii::t('main', 'Black'),
            'red'        => Yii::t('main', 'Red'),
            'yellow'     => Yii::t('main', 'Yellow'),
            'aqua'       => Yii::t('main', 'Aqua'),
            'navy'       => Yii::t('main', 'Navy'),
            'blue'       => Yii::t('main', 'Blue'),
            'light-blue' => Yii::t('main', 'Light Blue'),
            'green'      => Yii::t('main', 'Green'),
            'teal'       => Yii::t('main', 'Teal'),
            'olive'      => Yii::t('main', 'Olive'),
            'lime'       => Yii::t('main', 'Lime'),
            'orange'     => Yii::t('main', 'Orange'),
            'fuchsia'    => Yii::t('main', 'Fuchsia'),
            'purple'     => Yii::t('main', 'Purple'),
            'maroon'     => Yii::t('main', 'Maroon'),
        ];
    }

    public static function getColors()
    {
        return [
            'default' => Yii::t('main', 'Gray'),
            'danger'  => Yii::t('main', 'Red'),
            'primary' => Yii::t('main', 'Blue'),
            'success' => Yii::t('main', 'Green'),
            'warning' => Yii::t('main', 'Orange'),
        ];
    }
}
