<?php

namespace core\services\menu;

use core\entities\menu\MenuItem;
use core\forms\menu\MenuItemForm;
use core\repositories\menu\MenuItemRepository;

class MenuItemManageService
{
    private MenuItemRepository $repository;

    public function __construct(MenuItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(MenuItemForm $form): MenuItem
    {
        $menu = MenuItem::create(
        	$form,
        	$form->menu_id,
            $form->parent_id,
	        $form->target,
	        $form->css,
	        $form->style,
	        $form->module,
	        $form->url,
	        $form->url_params
        );
        $this->repository->save($menu);
        return $menu;
    }

    public function edit($id, MenuItemForm $form): void
    {
        $menu = $this->repository->get($id);
        $menu->edit(
        	$form,
	        $form->menu_id,
	        $form->parent_id,
	        $form->target,
	        $form->css,
	        $form->style,
	        $form->module,
	        $form->url,
	        $form->url_params
        );
        $this->repository->save($menu);
    }

    public function remove($id): void
    {
        $menu = $this->repository->get($id);
        $this->repository->remove($menu);
    }

    public function setPosition(array $position, $sort): void
    {
	    $item = $this->repository->get($position[0]);
	    $item->parent_id = $position[1] ?: null;
	    $item->sort = $sort;
	    $this->repository->save($item);
    }
}
