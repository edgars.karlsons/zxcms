<?php
/**
 * Created by Error202
 * Date: 03.09.2018
 */

namespace core\services\newsletter;

class FakeSubscribe implements Newsletter
{
    public function subscribe($email): void
    {
        return;
    }

    public function unsubscribe($email): void
    {
        return;
    }
}
