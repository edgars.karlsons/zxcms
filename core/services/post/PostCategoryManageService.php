<?php

namespace core\services\post;

use core\entities\Meta;
use core\entities\post\PostCategory;
use core\forms\post\PostCategoryForm;
use core\repositories\post\PostCategoryRepository;
use core\repositories\post\PostRepository;
use DomainException;

class PostCategoryManageService
{
    private PostCategoryRepository $categories;
    private PostRepository $posts;

    public function __construct(PostCategoryRepository $categories, PostRepository $posts)
    {
        $this->categories = $categories;
        $this->posts = $posts;
    }

    public function create(PostCategoryForm $form): PostCategory
    {
        $category = PostCategory::create(
            $form->name,
            $form->slug,
            $form->title,
            $form->description,
            $form->sort,
            $form->type_id,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );
        $this->categories->save($category);
        return $category;
    }

    public function edit($id, PostCategoryForm $form): void
    {
        $category = $this->categories->get($id);
        $category->edit(
            $form->name,
            $form->slug,
            $form->title,
            $form->description,
            $form->sort,
            $form->type_id,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );
        $this->categories->save($category);
    }

    public function remove($id): void
    {
        $category = $this->categories->get($id);
        if ($this->posts->existsByCategory($category->id)) {
            throw new DomainException('Unable to remove category with posts.');
        }
        $this->categories->remove($category);
    }
}
