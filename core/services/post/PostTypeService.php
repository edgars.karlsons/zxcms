<?php

namespace core\services\post;

use core\entities\post\PostType;
use core\forms\post\PostTypeForm;
use core\repositories\post\PostTypeRepository;

class PostTypeService
{
    private PostTypeRepository $types;

    public function __construct(PostTypeRepository $types)
    {
        $this->types = $types;
    }

    public function create(PostTypeForm $form): PostType
    {
        $type = PostType::create(
            $form->name,
            $form->singular,
            $form->plural
        );
        $this->types->save($type);
        return $type;
    }

    public function edit($id, PostTypeForm $form): void
    {
        $type = $this->types->get($id);
        $type->edit(
            $form->name,
            $form->singular,
            $form->plural
        );
        $this->types->save($type);
    }

    public function remove($id): void
    {
        $tag = $this->types->get($id);
        $this->types->remove($tag);
    }
}
