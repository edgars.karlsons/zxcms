<?php

namespace core\services\user;

use core\entities\user\User;
use core\forms\user\UserForm;
use core\repositories\user\UserRepository;
use core\services\RoleManager;
use core\services\TransactionManager;
use Yii;
use yii\base\Exception;

class UserManageService
{
    private UserRepository $repository;
    private RoleManager $roles;
    private TransactionManager $transaction;
    /**
     * @var //Newsletter
     */
    //private $newsletter;

    /**
     * UserManageService constructor.
     *
     * @param UserRepository $repository
     * @param RoleManager $roles
     * @param TransactionManager $transaction
     */
    public function __construct(
        UserRepository $repository,
        RoleManager $roles,
        TransactionManager $transaction
        //Newsletter $newsletter
    )
    {
        $this->repository  = $repository;
        $this->roles       = $roles;
        $this->transaction = $transaction;
        //$this->newsletter = $newsletter;
    }

    /**
     * @param UserForm $form
     * @return User
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function create(UserForm $form): User
    {
        $user = User::create(
            $form->username,
            $form->email,
            $form->password
        );
        $this->transaction->wrap(function () use ($user, $form) {
            $this->repository->save($user);
            $this->roles->assign($user->id, $form->role);
            //$this->newsletter->subscribe($user->email);
        });

        return $user;
    }

    /**
     * @param $id
     * @param UserForm $form
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function edit($id, UserForm $form): void
    {
        $user = $this->repository->get($id);
        $user->edit(
            $form->username,
            $form->email,
            $form->password
        );
        $this->transaction->wrap(function () use ($user, $form) {
            $this->repository->save($user);
            $this->roles->assign($user->id, $form->role);
        });
    }

    public function setBackendLanguage($language): void
    {
        if (in_array($language, array_keys(Yii::$app->params['backendTranslatedLanguages']))) {
            $user                   = $this->repository->get(Yii::$app->user->id);
            $user->backend_language = $language;
            $this->repository->save($user);
        }
    }

    /**
     * @param $id
     * @param $role
     * @throws \Exception
     */
    public function assignRole($id, $role): void
    {
        $user = $this->repository->get($id);
        $this->roles->assign($user->id, $role);
    }

    public function remove($id): void
    {
        $user = $this->repository->get($id);
        $this->repository->remove($user);
        //$this->newsletter->unsubscribe($user->email);
    }
}