<?php
/**
 * Created by Error202
 * Date: 20.08.2018
 */

namespace core\components\modules;

use core\entities\ModuleRecord;
use core\services\ModuleService;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

class ModuleManager
{
    public array $moduleNames = [];
    public array $modules = [];

    private ModuleService $service;

    public function __construct(ModuleService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function appendToMigrationTable($name)
    {
        $time       = time();
        $connection = Yii::$app->getDb();
        $command    = $connection->createCommand("INSERT INTO migration (version, apply_time) VALUE ('$name', '$time')");
        $command->execute();
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function removeFromMigrationTable($name)
    {
        $connection = Yii::$app->getDb();
        $command    = $connection->createCommand("DELETE FROM migration WHERE version = '$name'");
        $command->execute();
    }

    public function getModules(): array
    {
        $modules      = [];
        $localModules = $this->getLocalModules();
        foreach ($localModules as $local_module) {
            $db_module = ModuleRecord::find()->andWhere(['name' => $local_module['name']])->one();
            if ($this->isTableExist('modules') && !$db_module) {
                $db_module              = $this->service->create($local_module['name'], 'common\\modules\\' . $local_module['name'] . '\\' . $local_module['module']);
            }
            $db_module->description = $local_module['description'];
            $modules[]              = $db_module;
        }

        return $modules;
    }

    public function getLocalModules(): array
    {
        $this->getLocalModulesNames();

        if (empty($this->modules)) {
            foreach ($this->moduleNames as $module_name) {
                $manifest = Yii::getAlias('@common/modules/' . $module_name . '/manifest.php');
                if (file_exists($manifest)) {
                    $this->modules[] = require $manifest;
                }
            }
        }

        return $this->modules;
    }

    public function isExists($name): bool
    {
        $this->getLocalModules();
        $modules = ArrayHelper::getColumn($this->modules, 'name');
        return in_array($name, $modules);
    }

    private function getLocalModulesNames(): void
    {
        if (!empty($this->moduleNames)) {
            return;
        }

        $names      = [];
        $modulePath = Yii::getAlias('@common/modules');
        $modules    = file_exists($modulePath) ? FileHelper::findDirectories($modulePath, [
            'recursive' => false,
        ]) : [];
        foreach ($modules as $module) {
            $module  = basename($module);
            $names[] = $module;
        }
        $this->moduleNames = $names;
    }

    public function isTableExist($name): bool
    {
        return Yii::$app->db->schema->getTableSchema($name) !== null;
    }
}
