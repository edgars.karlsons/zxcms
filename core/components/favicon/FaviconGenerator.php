<?php
/**
 * Created by Error202
 * Date: 03.10.2017
 */

namespace core\components\favicon;

use Yii;
use yii\imagine\Image;
use yii\web\View;

class FaviconGenerator
{
    public string $originPath;
    public string $cachePath;

    public function __construct()
    {
        $this->originPath = Yii::getAlias('@staticRoot') . '/origin/favicon';
        $this->cachePath = Yii::getAlias('@staticRoot') . '/cache/favicon';
    }

    public function generateIcons()
    {
        if (!file_exists($this->originPath)) {
            mkdir($this->originPath, 0777, true);
        }

        if (!file_exists($this->cachePath)) {
            mkdir($this->cachePath, 0777, true);
        }

        if (file_exists($this->originPath . '/favicon.png')) {
            $this->generateFavicons();
            $this->generateAndroid();
            $this->generateApple();
            $this->generateMicrosoft();
            $this->generateManifestJson();
            $this->generateBrowserConfigXml();
        }
    }

    protected function generateFavicons()
    {
        foreach ([16, 32, 96, 194] as $s) {
            $filename = sprintf('favicon-%sx%s.png', $s, $s);

            Image::thumbnail($this->originPath . '/favicon.png', $s, $s)
                 ->save($this->cachePath . '/' . $filename);
        }
    }

    protected function generateAndroid()
    {
        foreach ([36, 48, 72, 96, 144, 192] as $s) {
            $filename = sprintf('android-chrome-%sx%s.png', $s, $s);

            Image::thumbnail($this->originPath . '/favicon.png', $s, $s)
                 ->save($this->cachePath . '/' . $filename);
        }
    }

    protected function generateApple()
    {
        foreach ([57, 60, 72, 76, 114, 120, 144, 152, 180] as $s) {
            $filename = sprintf('apple-touch-icon-%sx%s.png', $s, $s);

            Image::thumbnail($this->originPath . '/favicon.png', $s, $s)
                 ->save($this->cachePath . '/' . $filename);

            if ($s == 180) {
                $filename = sprintf('apple-touch-icon.png', $s, $s);

                Image::thumbnail($this->originPath . '/favicon.png', $s, $s)
                     ->save($this->cachePath . '/' . $filename);
            }
        }
    }

    protected function generateMicrosoft()
    {
        foreach ([70, 150, 270, 310] as $s) {
            $filename = sprintf('mstile-%sx%s.png', $s, $s);

            Image::thumbnail($this->originPath . '/favicon.png', $s, $s)
                 ->save($this->cachePath . '/' . $filename);
        }
    }

    protected function generateManifestJson()
    {
        $cacheUrl = Yii::getAlias('@static') . '/cache';

        // frontend
        file_put_contents(
            Yii::getAlias('@frontend/web') . '/' . 'manifest.json',
            (new View())->render('@core/components/favicon/manifest.php', [
                'appName' => Yii::$app->params['settings']['site']['name'] ?? Yii::$app->name,
                'iconPath' => $cacheUrl . '/favicon'
            ])
        );

        // backend
        file_put_contents(
            Yii::getAlias('@backend/web') . '/' . 'manifest.json',
            (new View())->render('@core/components/favicon/manifest.php', [
                'appName' => Yii::$app->params['settings']['site']['name'] ?? Yii::$app->name,
                'iconPath' => $cacheUrl . '/favicon'
            ])
        );
    }

    protected function generateBrowserConfigXml()
    {
        $cacheUrl = Yii::getAlias('@static') . '/cache';

        // frontend
        file_put_contents(
            Yii::getAlias('@frontend/web') . '/' . 'browserconfig.xml',
            (new View())->render('@core/components/favicon/browserconfig.php', [
                'iconPath' => $cacheUrl . '/favicon'
            ])
        );

        // backend
        file_put_contents(
            Yii::getAlias('@backend/web') . '/' . 'browserconfig.xml',
            (new View())->render('@core/components/favicon/browserconfig.php', [
                'iconPath' => $cacheUrl . '/favicon'
            ])
        );
    }
}
