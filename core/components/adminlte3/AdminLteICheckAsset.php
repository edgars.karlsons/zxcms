<?php
/**
 * Created by Error202
 * Date: 02.02.2020
 */

namespace core\components\adminlte3;

use yii\web\AssetBundle;

class AdminLteICheckAsset extends AssetBundle
{
	public $sourcePath = '@core/components/adminlte3/plugins/icheck-bootstrap';

	public $css = [
		'icheck-bootstrap.min.css',
	];
}
