<?php
/**
 * Created by Error202
 * Date: 26.10.2017
 */

namespace core\components\adminlte3\themes;

use yii\web\AssetBundle;

class AdminLteDarkAsset extends AssetBundle
{
    public $sourcePath = '@core/components/adminlte3/themes';

    public $css = [
        'css/bootstrap4-dark.css',
        'css/skin-dark.css',
        'css/site-dark.css',
    ];
}
