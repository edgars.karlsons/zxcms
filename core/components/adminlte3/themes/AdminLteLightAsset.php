<?php
/**
 * Created by Error202
 * Date: 26.10.2017
 */

namespace core\components\adminlte3\themes;

use yii\web\AssetBundle;

class AdminLteLightAsset extends AssetBundle
{
    public $sourcePath = '@core/components/adminlte3/themes';

    public $css = [
        'css/skin.css',
    ];
}
