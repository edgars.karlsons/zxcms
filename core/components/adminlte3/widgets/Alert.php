<?php
/**
 * Created by Error202
 * Date: 02.02.2020
 */

namespace core\components\adminlte3\widgets;

use yii\bootstrap\Widget;
use Yii;

class Alert extends Widget
{
	/**
	 * Иконки событий
	 * @var array
	 */
	public $alertIcons = [
		'error'   => 'fas fa-times-circle',
		'danger'  => 'fas fa-times-circle',
		'success' => 'far fa-check-circle',
		'info'    => 'fas fa-info',
		'warning' => 'fas fa-exclamation-triangle'
	];

	/**
	 * Наименования событий.
	 * @var array
	 */
	public $alertMeanings = [
		'error'   => '',
		'danger'  => '',
		'success' => '',
		'info'    => '',
		'warning' => ''
	];

	/**
	 * @var array the alert types configuration for the flash messages.
	 * This array is setup as $key => $value, where:
	 * - $key is the name of the session flash variable
	 * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
	 */
	public $alertTypes = [
		'error'   => 'alert-danger',
		'danger'  => 'alert-danger',
		'success' => 'alert-success',
		'info'    => 'alert-info',
		'warning' => 'alert-warning'
	];

	/**
	 * @var array the options for rendering the close button tag.
	 */
	public $closeButton = [];

	public function init()
	{
		parent::init();

		$session = Yii::$app->session;
		$flashes = $session->getAllFlashes();
		$appendCss = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

		foreach ($flashes as $type => $data) {
			if (isset($this->alertTypes[$type])) {
				$data = (array) $data;
				foreach ($data as $i => $message) {
					/* initialize css class for each alert box */
					$this->options['class'] = $this->alertTypes[$type] . $appendCss;

					/* assign unique id to each alert box */
					$this->options['id'] = $this->getId() . '-' . $type . '-' . $i;

					$appendIcon = isset($this->alertIcons[$type]) ? ' <i class="' . $this->alertIcons[$type] . '" aria-hidden="true"></i> ' : '';

					echo '
<div class="alert ' . $this->options['class'] .'" role="alert">
        <strong>' . $appendIcon . $this->alertMeanings[$type] . '</strong> ' . $message . '
    </div>';
				}

				$session->removeFlash($type);
			}
		}
	}
}
