<?php

namespace core\forms\post;

use core\entities\post\Post;
use core\entities\post\PostTag;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * @property array $newNames
 */
class PostTagForm extends Model
{
    public array $existing = [];
    public string $textNew;
    public array $new_tags;

    public function __construct(Post $post = null, $config = [])
    {
        if ($post) {
            $this->existing = ArrayHelper::getColumn($post->postTagAssignments, 'tag_id');
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            ['existing', 'each', 'rule' => ['integer']],
            [['textNew'], 'string'],
            ['existing', 'default', 'value' => []],
	        ['new_tags', 'each', 'rule' => ['string']],
        ];
    }

    public function tagsList(int $type_id): array
    {
        return ArrayHelper::map(PostTag::find()->andWhere(['type_id' => $type_id])->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    public function getNewNames(): array
    {
        return array_filter(array_map('trim', preg_split('#\s*,\s*#i', $this->textNew)));
    }
}
