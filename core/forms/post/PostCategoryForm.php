<?php

namespace core\forms\post;

use core\entities\post\PostCategory;
use core\forms\CompositeForm;
use core\forms\MetaForm;
use core\validators\SlugValidator;
use Yii;

/**
 * @property MetaForm $meta;
 */
class PostCategoryForm extends CompositeForm
{
    public string $name;
    public string $slug;
    public string $title;
    public string $description;
    public int $sort;
    public int $type_id;

    private PostCategory $category;

    public function __construct(PostCategory $category = null, $config = [])
    {
        if ($category) {
            $this->name = $category->name;
            $this->slug = $category->slug;
            $this->title = $category->title;
            $this->description = $category->description;
            $this->sort = $category->sort;
	        $this->type_id = $category->type_id;
            $this->meta = new MetaForm($category->meta);
            $this->category = $category;
        } else {
            $this->meta = new MetaForm();
            $this->sort = PostCategory::find()->max('sort') + 1;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug', 'title'], 'string', 'max' => 255],
	        ['type_id', 'integer'],
            [['description'], 'string'],
            ['slug', SlugValidator::class],
            [['name', 'slug'], 'unique', 'targetClass' => PostCategory::class, 'filter' => $this->category ? ['<>', 'id', $this->category->id] : null]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('post', 'Name'),
            'slug' => Yii::t('post', 'SEO link'),
            'sort' => Yii::t('post', 'Sort'),
            'title' => Yii::t('post', 'Title'),
            'description' => Yii::t('post', 'Description'),
        ];
    }

    public function updateSort() {
	    $this->sort = PostCategory::find()->where(['type_id' => $this->type_id])->max('sort') + 1;
    }

    public function internalForms(): array
    {
        return ['meta'];
    }
}
