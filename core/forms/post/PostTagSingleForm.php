<?php

namespace core\forms\post;

use core\entities\post\PostTag;
use core\validators\SlugValidator;
use yii\base\Model;
use Yii;

class PostTagSingleForm extends Model
{
    public string $name;
    public string $slug;
    public int $type_id;

    private PostTag $tag;

    public function __construct(PostTag $tag = null, $config = [])
    {
        if ($tag) {
            $this->name = $tag->name;
            $this->slug = $tag->slug;
            $this->type_id = $tag->type_id;
            $this->tag = $tag;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
	        ['type_id', 'integer'],
            ['slug', SlugValidator::class],
            [['name', 'slug'], 'unique', 'targetClass' => PostTag::class, 'filter' => $this->tag ? ['<>', 'id', $this->tag->id] : null]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('post', 'Tag Name'),
            'slug' => Yii::t('post', 'SEO link'),
        ];
    }
}
