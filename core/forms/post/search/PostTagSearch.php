<?php

namespace core\forms\post\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use core\entities\post\PostTag;

class PostTagSearch extends Model
{
    public $id;
    public $name;
    public $slug;
    public $tid;

    public function rules(): array
    {
        return [
            [['id', 'tid'], 'integer'],
            [['name', 'slug'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
	    $this->tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT);
        $query = PostTag::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['name' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->tid,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
