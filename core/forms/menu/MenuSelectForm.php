<?php

namespace core\forms\menu;

use yii\base\Model;
use Yii;

class MenuSelectForm extends Model
{
    public ?int $id = null;

    public function rules(): array
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
        ];
    }

	public function attributeLabels() {
		return [
			'id' => Yii::t('menu', 'Menu'),
		];
	}
}
