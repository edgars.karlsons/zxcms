<?php

namespace core\forms\menu;

use core\components\LanguageDynamicModel;
use core\entities\menu\Menu;
use Yii;

class MenuForm extends LanguageDynamicModel
{
    public ?string $name = null;

    private Menu $menu;

    public function __construct( Menu $menu = null, array $attributes = [], array $config = [] ) {
	    if ($menu) {
		    $this->menu = $menu;
	    }
	    parent::__construct( $menu, $attributes, $config );
    }

    public function rules(): array
    {
	    return array_merge(
		    parent::rules(),
	        [
	            [['name'], 'required'],
	            [['name'], 'string', 'max' => 255],
	        ]
	    );
    }

	public function attributeLabels(): array
    {
		return array_merge(
			parent::attributeLabels(),
			[
				'name' => Yii::t('main', 'Name'),
			]
		);
	}
}
