<?php

namespace core\forms;

use core\components\LanguageDynamicModel;
use core\entities\Meta;
use Yii;

class MetaForm extends LanguageDynamicModel
{
    public ?string $title = null;
    public ?string $description = null;
    public ?string $keywords = null;

    public function __construct(Meta $meta = null, $config = [])
    {
        if ($meta) {
            $this->title = $meta->title;
            $this->description = $meta->description;
            $this->keywords = $meta->keywords;
        }
        parent::__construct($config);
	    // fill translate values
	    /*if ($meta) {
		    foreach ( $meta->translations as $translate ) {
			    $this->{'title' . '_' . $translate->language} = $translate->title;
			    $this->{'description' . '_' . $translate->language} = $translate->description;
			    $this->{'keywords' . '_' . $translate->language} = $translate->keywords;
		    };
	    };*/
    }

    public function rules(): array
    {
	    return array_merge(
		    parent::rules(),
		    [
	            [['title'], 'string', 'max' => 255],
	            [['description', 'keywords'], 'string'],
	        ]
	    );
    }

    public function attributeLabels(): array
    {
	    return array_merge(
		    parent::attributeLabels(),
		    [
		        'title' => Yii::t('main', 'Title'),
		        'description' => Yii::t('main', 'Description'),
		        'keywords' => Yii::t('main', 'Keywords'),
		    ]
	    );
    }
}
