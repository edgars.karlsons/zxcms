<?php
namespace core\forms\auth;

use Yii;
use yii\base\Model;
use core\entities\user\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public ?string $username = null;
    public ?string $email = null;
    public ?string $password = null;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => User::class, 'message' => Yii::t('auth', 'This username has already been taken.')],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::class, 'message' => Yii::t('auth', 'This email address has already been taken.')],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels() {
	    return [
	        'username' => Yii::t('auth', 'Username'),
	        'email' => Yii::t('auth', 'E-mail'),
	        'password' => Yii::t('auth', 'Password'),
	    ];
    }
}
