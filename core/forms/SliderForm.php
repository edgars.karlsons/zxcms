<?php

namespace core\forms;

use core\entities\Slider;
use yii\base\Model;
use Yii;
use yii\validators\ImageValidator;
use yii\web\UploadedFile;


class SliderForm extends Model
{
    public ?string $title = null;
    public ?string $tagline = null;
    public string|UploadedFile|null $image = null;
    public ?string $url = null;
    public ?int $sort = null;

    public function __construct(Slider $slider = null, $config = [])
    {
        if ($slider) {
            $this->title = $slider->title;
            $this->tagline = $slider->tagline;
            $this->url = $slider->url;
            $this->sort = $slider->sort;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
    	$imageValidator = new ImageValidator();
    	$maxSize = $imageValidator->getSizeLimit() < 20*1024*1024 ? $imageValidator->getSizeLimit() : 20*1024*1024;
        return [
            [['title', 'image'], 'required', 'on' => Slider::SCENARIO_CREATE],
	        [['title'], 'required', 'on' => Slider::SCENARIO_UPDATE],
            [['sort'], 'integer'],
	        [['title'], 'string', 'max' => 128],
            [['tagline', 'url'], 'string', 'max' => 255],
	        [['image'], 'image', 'extensions' => 'jpg,png,gif', 'skipOnEmpty' => true, 'maxSize' => $maxSize],
        ];
    }

	public function attributeLabels() {
		return [
			'title' => Yii::t('slider', 'Title'),
			'tagline' => Yii::t('slider', 'Tagline'),
			'image' => Yii::t('slider', 'Image'),
			'url' => Yii::t('slider', 'URL'),
			'sort' => Yii::t('slider', 'Sort'),
		];
	}

	public function beforeValidate(): bool
    {
		if (parent::beforeValidate()) {
			$this->image = UploadedFile::getInstance($this, 'image');
			return true;
		}
		return false;
	}
}
