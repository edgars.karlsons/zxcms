<?php

namespace core\repositories;

use core\entities\ModuleRecord;
use RuntimeException;
use yii\db\StaleObjectException;

class ModuleRepository
{
    public function get($id): ModuleRecord
    {
        if (!$module = ModuleRecord::findOne($id)) {
            throw new NotFoundException('Module is not found.');
        }
        return $module;
    }

    public function save(ModuleRecord $module): void
    {
        if (!$module->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param ModuleRecord $module
     * @throws StaleObjectException
     */
    public function remove(ModuleRecord $module): void
    {
        if (!$module->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
