<?php
/**
 * Created by Error202
 * Date: 09.07.2018
 */

namespace core\entities\menu;

use core\behaviors\LanguageBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class MenuItem
 * @package core\entities\menu
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $parent_id
 * @property string $name
 * @property string $title_attr
 * @property string $target
 * @property string $css
 * @property string $style
 * @property string $module
 * @property string $url
 * @property string $url_params
 * @property integer $sort
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] translations
 * @property ActiveRecord[] translation
 *
 * @property MenuItem $parent
 * @property MenuItem[] $children
 * @property Menu $menu
 */

class MenuItem extends ActiveRecord
{
	public $_form;

	public static function create($form, $menu_id, $parent_id, $target, $css, $style, $module, $url, $url_params): self
	{
		$menu = new static();
		$menu->menu_id = $menu_id;
		$menu->parent_id = $parent_id;
		$menu->target = $target;
		$menu->css = $css;
		$menu->style = $style;
		$menu->module = $module;
		$menu->url = $url;
		$menu->url_params = $url_params;
		$menu->_form = $form;
		return $menu;
	}

	public function edit($form, $menu_id, $parent_id, $target, $css, $style, $module, $url, $url_params): void
	{
		$this->menu_id = $menu_id;
		$this->parent_id = $parent_id;
		$this->target = $target;
		$this->css = $css;
		$this->style = $style;
		$this->module = $module;
		$this->url = $url;
		$this->_form = $form;
		$this->url_params = $url_params;
	}

	public static function tableName(): string
	{
		return '{{%menu_items}}';
	}

	/*public function behaviors()
	{
		return [
			[
				'class' => SortableBehavior::class,
				'query' => ['menu_id'],
			],
		];
	}*/

	public function beforeSave( $insert ) {
		if (parent::beforeSave($insert)) {
			if (!isset($this->sort)) {
				$count = MenuItem::find()
		                      ->andWhere( [ 'menu_id' => $this->menu_id ] )
		                      ->andWhere( [ 'parent_id' => $this->parent_id ] )
		                      ->count();
				$this->sort = $count;
			}
			return true;
		}
		return false;
	}

	public function beforeDelete(){
		foreach($this->children as $child) {
			$child->delete();
		}
		return parent::beforeDelete();
	}

	public function getUrl(): string
	{
		$params = $this->url_params ? Json::decode($this->url_params, true) : [];
		return Url::to(array_merge([$this->url], $params));
	}

	public function isActive(): bool
	{
		return \Yii::$app->request->getUrl() == $this->getUrl();
	}

	public function getChildren()
	{
		return $this->hasMany(MenuItem::class, ['parent_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
	}

	public function hasChildren(): bool
	{
		return $this->hasMany(MenuItem::class, ['parent_id' => 'id'])->orderBy(['sort' => SORT_ASC])->count() > 0 ? true : false;
	}

	public function getParent()
	{
		return $this->hasOne(MenuItem::class, ['id' => 'parent_id']);
	}

	public function getMenu()
	{
		return $this->hasOne(Menu::class, ['id' => 'menu_id']);
	}

	public function behaviors()
	{
		return [
			[
				'class' => LanguageBehavior::class,
				'virtualClassName' => 'MenuItemVirtualTranslate',
				'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
				'relativeField' => 'menu_item_id',
				'tableName' => "{{%menu_items_lng}}",
				'attributes' => ['name', 'title_attr'],
				'defaultLanguage' => \Yii::$app->params['defaultLanguage'],
			],
		];
	}
}