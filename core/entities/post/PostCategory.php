<?php

namespace core\entities\post;

use core\behaviors\MetaBehavior;
use core\entities\Meta;
use core\entities\post\queries\PostCategoryQuery;
use yii\db\ActiveRecord;
use yii\caching\TagDependency;
use Yii;


/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property integer $sort
 * @property string $meta_json
 * @property integer $type_id
 * @property Meta $meta
 */
class PostCategory extends ActiveRecord
{
    public $meta;

    public static function create($name, $slug, $title, $description, $sort, $type_id, Meta $meta): self
    {
        $category = new static();
        $category->name = $name;
        $category->slug = $slug;
        $category->title = $title;
        $category->description = $description;
        $category->sort = $sort;
        $category->type_id = $type_id;
        $category->meta = $meta;
        return $category;
    }

    public function edit($name, $slug, $title, $description, $sort, $type_id, Meta $meta): void
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->title = $title;
        $this->description = $description;
        $this->sort = $sort;
        $this->type_id = $type_id;
        $this->meta = $meta;
    }

    public function attributeLabels()
    {
        return [
	        'id' => Yii::t('post', 'ID'),
            'name' => Yii::t('post', 'Name'),
            'slug' => Yii::t('post', 'SEO link'),
            'sort' => Yii::t('post', 'Sort'),
            'title' => Yii::t('post', 'Title'),
            'description' => Yii::t('post', 'Description'),
            'meta.title' => Yii::t('post', 'Meta Title'),
            'meta.description' => Yii::t('post', 'Meta Description'),
            'meta.keywords' => Yii::t('post', 'Meta Keywords'),
        ];
    }

    public function getSeoTitle(): string
    {
        return $this->meta->title ?: $this->getHeadingTile();
    }

    public function getHeadingTile(): string
    {
        return $this->title ?: $this->name;
    }

    public function getPostsCount(): int
    {
    	return Post::find()->where(['category_id' => $this->id])->count('*');
    }

    public static function tableName(): string
    {
        return '{{%post_categories}}';
    }

    public function behaviors(): array
    {
        return [
            MetaBehavior::className(),
        ];
    }

	public static function find(): PostCategoryQuery
	{
		return new PostCategoryQuery(static::class);
	}
}