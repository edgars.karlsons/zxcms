<?php

namespace core\entities\post;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property string $name
 * @property string $singular
 * @property string $plural
 */
class PostType extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_types';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('post', 'ID'),
            'name' => Yii::t('post', 'Name'),
            'singular' => Yii::t('post', 'Singular'),
            'plural' => Yii::t('post', 'Plural'),
        ];
    }

	public static function create($name, $singular, $plural): self
	{
		$type = new static();
		$type->name = $name;
		$type->singular = $singular;
		$type->plural = $plural;
		return $type;
	}

	public function edit($name, $singular, $plural): void
	{
		$this->name = $name;
		$this->singular = $singular;
		$this->plural = $plural;
	}

}
