<?php

namespace core\entities\post\queries;

use core\entities\post\Post;
use yii\db\ActiveQuery;

class PostQuery extends ActiveQuery
{
    /**
     * @param null $alias
     * @return $this
     */
    public function active($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'status' => Post::STATUS_ACTIVE,
        ]);
    }

    public function byType($type)
    {
    	return $this->andWhere(['type_id' => $type]);
    }

    public function last()
    {
    	return $this->orderBy(['published_at' => SORT_DESC]);
    }

	public function popular()
	{
		return $this->orderBy(['views' => SORT_DESC]);
	}
}