<?php
/**
 * Created by Error202
 * Date: 26.01.2018
 */

namespace core\entities;

use yii\db\ActiveRecord;

/**
 * Class Session
 * @package entities
 *
 * @property string $id
 * @property integer $expire
 * @property string $data
 * @property integer $user_id
 * @property string $thd_id [bigint unsigned]
 * @property string $conn_id [bigint unsigned]
 * @property string $user [varchar(288)]
 * @property string $db [varchar(64)]
 * @property string $command [varchar(16)]
 * @property string $state [varchar(64)]
 * @property int $time [bigint]
 * @property string $current_statement
 * @property string $statement_latency [varchar(11)]
 * @property string $progress [decimal(26,2)]
 * @property string $lock_latency [varchar(11)]
 * @property string $rows_examined [bigint unsigned]
 * @property string $rows_sent [bigint unsigned]
 * @property string $rows_affected [bigint unsigned]
 * @property string $tmp_tables [bigint unsigned]
 * @property string $tmp_disk_tables [bigint unsigned]
 * @property string $full_scan [varchar(3)]
 * @property string $last_statement
 * @property string $last_statement_latency [varchar(11)]
 * @property string $current_memory [varchar(11)]
 * @property string $last_wait [varchar(128)]
 * @property string $last_wait_latency [varchar(13)]
 * @property string $source [varchar(64)]
 * @property string $trx_latency [varchar(11)]
 * @property string $trx_state [enum('ACTIVE', 'COMMITTED', 'ROLLED BACK')]
 * @property string $trx_autocommit [enum('YES', 'NO')]
 * @property string $pid [varchar(1024)]
 * @property string $program_name [varchar(1024)]
 */

class Session extends ActiveRecord
{
	public static function tableName(): string
    {
		return '{{%session}}';
	}
}