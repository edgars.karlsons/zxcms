<?php

namespace core\listeners\user;

use core\services\newsletter\Newsletter;
use core\events\user\UserSignUpConfirmed;

class UserSignupConfirmedListener
{
    private Newsletter $newsletter;

    public function __construct(Newsletter $newsletter)
    {
        $this->newsletter = $newsletter;
    }

    public function handle(UserSignUpConfirmed $event): void
    {
        $this->newsletter->subscribe($event->user->email);
    }
}
