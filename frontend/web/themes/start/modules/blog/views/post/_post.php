<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\blog\entities\BlogPost */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\StringHelper;

$url = Url::to(['/blog/post/post', 'id' =>$model->id]);
$url_full = Yii::$app->params['frontendHostInfo'] . Url::to(['/blog/post/post', 'id' =>$model->id]);
?>

<!-- Blog Post -->
<div class="card mb-4">
	<a href="<?= $url ?>" title="<?= Html::encode($model->translation->title) ?>"><img class="card-img-top" src="<?= Html::encode($model->getThumbFileUrl('image', 'blog_list')) ?>" alt="<?= Html::encode($model->translation->title) ?>"></a>
	<div class="card-body">
		<h2 class="card-title"><?= Html::encode($model->translation->title) ?></h2>
		<p class="card-text"><?= StringHelper::truncateWords(Html::encode($model->translation->description), 30, '...') ?></p>
		<a href="<?= $url ?>" class="btn btn-primary" title="<?= Html::encode($model->translation->title) ?>"><?= Yii::t('blog_public', 'Read More') ?> &rarr;</a>
	</div>
	<div class="card-footer text-muted">
		<time datetime="<?= date('Y') ?>"><?= Yii::$app->formatter->asDate($model->published_at, 'php:d F, Y') ?></time>
	</div>
</div>
