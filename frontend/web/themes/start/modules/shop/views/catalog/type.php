<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $type \app\modules\shop\entities\ProductType */

use yii\helpers\Html;

$this->title = $type->getSeoTitle();

$this->registerMetaTag(['name' =>'description', 'content' => $type->meta->description]);
$this->registerMetaTag(['name' =>'keywords', 'content' => $type->meta->keywords]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('shop_public', 'Catalog'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $type->name;
?>

<h2 class="page_heading"><?= Html::encode($type->name) ?></h2>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>


