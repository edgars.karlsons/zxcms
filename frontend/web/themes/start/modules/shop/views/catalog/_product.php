<?php

/* @var $this yii\web\View */
/* @var $product \common\modules\shop\entities\product\ShopProduct */

/* @var $itemClass string */

use common\modules\shop\helpers\PriceHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use common\modules\shop\helpers\ShopProductHelper;

$url       = Url::to(['product', 'id' => $product->id]);
$inCompare = in_array($product->id, Yii::$app->session->get('compare', []));

// todo wishlist check
$inWish = false; //Yii::$app->user->isGuest ? false : Yii::$app->user->identity->user->inWishlist($product->id);

$priceNew = $product->getCalculatedNewPrice();
$priceOld = $product->getCalculatedOldPrice();
?>

<div class="product col-sm-4 product_collection <?= $itemClass ?>">

    <div class="product_wrapper">

        <?php if ($product->label && $product->label > 0) : ?>
            <div class="ribbon ribbon_<?= $product->label ?>">
                <span><?= ShopProductHelper::labelName($product->label) ?></span></div>
        <?php endif; ?>

        <figure class="card card-product">

            <?php if ($product->getCalculatedOldPrice()) : ?>
                <span class="badge-offer"><b><?= round(100 - ($product->getCalculatedNewPrice() / $product->getCalculatedOldPrice()) * 100) ?>%</b></span>
            <?php endif; ?>

            <!-- <span class="badge-offer"><b> - 50%</b></span>-->
            <div class="img-wrap product_img">
                <a class="img_change" href="<?= Html::encode($url) ?>" title="<?= Html::encode($product->translation->name) ?>">
                    <img src="<?= Html::encode($product->mainPhoto->getThumbFileUrl('file', 'catalog_list')) ?>">
                    <?php if (isset($product->photos[1])) : ?>
                        <img class="img__2"
                             src="<?= Html::encode($product->photos[1]->getThumbFileUrl('file', 'catalog_list')) ?>"
                             alt="<?= Html::encode($product->translation->name) ?>" style="opacity: 0;">
                    <?php endif; ?>
                </a>
            </div>

            <figcaption class="info-wrap">
                <a href="<?= Html::encode($url) ?>" class="title"><?= Html::encode($product->translation->name) ?></a>
                <div class="action-wrap">


                    <!--<a href="#" class="btn btn-primary btn-sm float-right"> Order </a> -->

                    <?php if (!$product->modifications) : ?>
                        <?= Html::a('
                    <span class="fa-stack float-right">
                        <i class="fa fa-square fa-stack-2x"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span>',
                            ['/shop/cart/add', 'id' => $product->id],
                            [
                                'class'             => 'link-blue sjax',
                                'title'             => Yii::t('shop_public', 'Add to Cart'),
                                'data-sjax-id'      => 'sj-cart-widget',
                                'data-sjax-method'  => 'post',
                                'data-sjax-type'    => 'success',
                                'data-sjax-message' => Yii::t('shop_public', 'Product added to cart.'),
                            ]
                        )
                        ?>
                    <?php else : ?>
                        <?= Html::a('
                <span class="fa-stack float-right">
                    <i class="fa fa-square fa-stack-2x"></i>
                    <i class="fa fa-bars fa-stack-1x fa-inverse"></i>
                </span>',
                            ['/shop/catalog/product', 'id' => $product->id],
                            [
                                'class' => 'link-blue',
                                'title' => Yii::t('shop_public', 'Specify options'),
                            ]
                        )
                        ?>
                    <?php endif; ?>

                    <?php if (!Yii::$app->user->isGuest) : ?>
                        <span id="sj-favorite-<?= $product->id ?>">
                        <?= Html::a('
                                <span class="fa-stack float-right">
                                    <i class="fa fa-square fa-stack-2x"></i>
                                    <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                                </span>',
                            $inWish ? ['/shop/wishlist/delete', 'id' => $product->id] : ['/shop/wishlist/add', 'id' => $product->id],
                            [
                                'class'             => $inWish ? "link-red" . " sjax" : "link-blue" . " sjax",
                                'title'             => $inWish ? Yii::t('shop_public', 'Remove from Wish') : Yii::t('shop_public',
                                    'Add to Wish'),
                                'data'              => [
                                    'method' => 'post',
                                ],
                                'data-sjax-id'      => 'sj-favorite-' . $product->id,
                                'data-sjax-method'  => 'post',
                                'data-sjax-type'    => 'success',
                                'data-sjax-message' => $inWish ? Yii::t('shop_public',
                                    'Product removed from wish list.') : Yii::t('shop_public', 'Product added to wish list.'),
                            ]
                        )
                        ?>
                        </span>
                    <?php endif; ?>


                    <div class="price-wrap h5">
                        <span class="price-new"><?= PriceHelper::format($priceNew) ?></span>
                        <br>
                        <del class="price-old" style="font-size: 12px;"><?= PriceHelper::format($priceOld) ?></del>
                    </div> <!-- price-wrap.// -->
                </div> <!-- action-wrap -->
            </figcaption>
        </figure>

        <div class="clearfix"></div>
    </div>

</div>