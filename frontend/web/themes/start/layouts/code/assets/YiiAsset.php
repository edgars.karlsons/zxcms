<?php
/**
 * Created by Error202
 * Date: 06.06.2018
 */

namespace frontend\web\themes\start\layouts\code\assets;

use yii\web\AssetBundle;

class YiiAsset extends AssetBundle
{
	public $sourcePath = '@yii/assets';
	public $js = [
		'yii.js',
	];
	public $depends = [
		'yii\web\JqueryAsset',
	];
}