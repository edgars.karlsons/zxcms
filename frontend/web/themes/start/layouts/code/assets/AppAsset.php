<?php
/**
 * Created by Error202
 * Date: 06.06.2018
 */

namespace frontend\web\themes\start\layouts\code\assets;

use frontend\assets\DeviceJsAsset;
use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/start';
    public $baseUrl = '@web/themes/start';
    public $css = [
        'css/main.css',
        'css/uikit.css',
        'css/responsive.css',
        'css/ribbon.css',
    ];
    public $js = [
        'js/images.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        BootstrapAsset::class,
        DeviceJsAsset::class,
    ];
}
