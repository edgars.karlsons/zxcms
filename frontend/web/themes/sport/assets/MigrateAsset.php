<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\web\themes\sport\assets;

use yii\web\AssetBundle;

class MigrateAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-migrate';
    public $js = [
        'jquery-migrate.js',
    ];
}
