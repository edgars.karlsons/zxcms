<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\web\themes\sport\assets;

use yii\web\AssetBundle;

class TemplateCssAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/themes/sport';
    public $css = [
        'css/main.css',
        'plugins/smartmenus/css/sm-core-css.css',
        'plugins/smartmenus/css/css/sm-sport-blue.css',
        'plugins/smartmenus/css/css/sm-simple.css',
        //'plugins/countdown/css/countdown.css',
    ];

    public function __construct($config = [])
    {
        if (YII_ENV_DEV) {
            $this->publishOptions['forceCopy'] = true;
        }
        parent::__construct($config);
    }

    public function registerAssetFiles($view)
    {
        parent::registerAssetFiles($view);

        $manager = $view->getAssetManager();
        $view->registerCssFile($manager->getAssetUrl($this, 'ie9.css'), ['condition' => 'lte IE9']);
    }
}
