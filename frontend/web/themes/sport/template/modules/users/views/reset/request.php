<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\users\forms\auth\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<h2 class="page_heading"><?= Yii::t('users_public', $this->title) ?></h2>

<div id="account_login">
    <div class="account_wrapper">
        <div class="account_section">




    <p class="note"><?= Yii::t('users_public', 'Please fill out your email. A link to reset password will be sent there.') ?></p>

    <div class="row">
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true])->label(Yii::t('users_public', 'E-mail')) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('users_public', 'Send'), ['class' => 'btn btn-blue']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('users_public', 'Cancel'), ['/users/auth/login']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>


        </div>
    </div>
</div>
