<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $tag \app\modules\shop\entities\Tag */

use yii\helpers\Html;

$this->title = $tag->name;

//$this->registerMetaTag(['name' =>'description', 'content' => $brand->meta->description]);
//$this->registerMetaTag(['name' =>'keywords', 'content' => $brand->meta->keywords]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('shop_public', 'Catalog'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $tag->name;
?>

<h2 class="page_heading"><?= Html::encode($tag->name) ?></h2>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>


