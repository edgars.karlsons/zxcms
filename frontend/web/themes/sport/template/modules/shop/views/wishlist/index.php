<?php

/* @var $this yii\web\View */

use app\modules\shop\entities\product\Product;
use app\modules\shop\helpers\PriceHelper;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('shop_public', 'Wish List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('users_public', 'Profile'), 'url' => ['/users/account/profile/edit']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-wishlist">
    <h2 class="page_heading"><?= Html::encode($this->title) ?></h2>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'label' => Yii::t('shop_public', 'Product Name'),
                'value' => function (Product $model) {

                    $img = Html::a(Html::img($model->mainPhoto->getThumbFileUrl('file', 'cart_list'), ['class' => 'img-thumbnail']), ['/shop/catalog/product', 'id' => $model->id]);
                    $img = '<div class="cart_item__img">' . $img . '</div>';


                    $info = '<div class="cart_item__info"><h3 class="cart_item__name product_name">' . Html::a(Html::encode($model->name), ['/shop/catalog/product', 'id' => $model->id]) . '</h3></div>';

                    return $img . $info;

                    //return $model->mainPhoto ? Html::img($model->mainPhoto->getThumbFileUrl('file', 'admin')) : null;
                },
                'format' => 'raw',
                //'contentOptions' => ['style' => 'width: 100px'],
                'contentOptions' => ['class' => 'cell_1'],
            ],
            [
                'attribute' => 'price_new',
                'label' => Yii::t('shop_public', 'Price'),
                'value' => function (Product $model) {
                    return '<span class="money">' . PriceHelper::format($model->price_new) . '</span>';
                },
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-right'],
                'headerOptions' => ['class' => 'cell_2'],
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{delete}',
                'options' => ['style' => 'width: 50px;'],
                'headerOptions' => ['class' => 'cell_3'],
            ],
        ],
        'tableOptions' => [
            'class' => 'cart_list'
        ],
        'rowOptions' => [
            'class' => 'cart_item'
        ],
        'showHeader' => true,
        'summary' => '',
    ]); ?>

</div>
