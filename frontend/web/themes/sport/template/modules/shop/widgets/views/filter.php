<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\shop\entities\Characteristic;
use app\modules\shop\helpers\FilterHelper;

/**
 * @var $this \yii\web\View
 * @var $searchForm \app\modules\shop\forms\search\SearchForm
 * @var $requestId integer
 * @var $brandId;
 * @var $typeId;
 * @var $tagId;
 * @var $filter array
 * @var $all array
 * @vat $types_count integer
 */
$doc_count = $filter['aggregations']['products_count']['value'];
?>

<?php if ($requestId): ?>
    <?php $form = ActiveForm::begin(['action' => ['/shop/catalog/category', 'id' => $requestId], 'method' => 'get']) ?>
<?php elseif ($brandId): ?>
    <?php $form = ActiveForm::begin(['action' => ['/shop/catalog/brand', 'id' => $brandId], 'method' => 'get']) ?>
<?php elseif ($tagId): ?>
    <?php $form = ActiveForm::begin(['action' => ['/shop/catalog/tag', 'id' => $tagId], 'method' => 'get']) ?>
<?php elseif ($typeId): ?>
    <?php $form = ActiveForm::begin(['action' => ['/shop/catalog/product-type', 'id' => $typeId], 'method' => 'get']) ?>
<?php else: ?>
    <?php $form = ActiveForm::begin(['action' => ['/shop/catalog/index'], 'method' => 'get']) ?>
<?php endif; ?>

<!-- < ?php print_r($filter); die;  ?> -->

<!-- BRANDS -->
<?php if (count($all['aggregations']['brands']['buckets']) > 1): ?>
    <?= $this->render('checkbox-filter', [
        'searchForm' => $searchForm,
        'items' => FilterHelper::checkboxBrends($filter['aggregations']['brands']['buckets'], $all['aggregations']['brands']['buckets']),
        'form' => $form,
        'attribute' => 'brand',
        'title' => Yii::t('shop_public', 'Vendor'),
    ]) ?>
<?php endif; ?>

<!-- PRISES -->
<?php if ($all['aggregations']['prices']['count'] > 1): ?>
    <?= $this->render('range-filter', [
        'searchForm' => $searchForm,
        //'items' => [$filter['aggregations']['prices']['min'] , $filter['aggregations']['prices']['max']],
        'items' => [$all['aggregations']['prices']['min'] , $all['aggregations']['prices']['max']],
        'form' => $form,
        'from' => [
            'attribute' => 'price_from',
            'id' => 'price_from',
            'value' => $searchForm->price_from,
        ],
        'to' => [
            'attribute' => 'price_to',
            'id' => 'price_to',
            'value' => $searchForm->price_to,
        ],
        'title' => Yii::t('shop_public', 'Price'),
    ]) ?>
<?php endif; ?>

<!-- TYPES -->

<!--
< ?php if (count($filter['aggregations']['types']['buckets']) > 1 || $searchForm->type): ?>
    < ?= $this->render('checkbox-filter', [
        'searchForm' => $searchForm,
        'items' => FilterHelper::checkboxTypes($filter['aggregations']['types']['buckets']),
        'form' => $form,
        'attribute' => 'type',
        'title' => Yii::t('shop_public', 'Product Type'),
    ]) ?>
< ?php endif; ?>
-->

<?php if ($types_count == 1 ): ?>

<?php foreach ($searchForm->values as $i => $value): ?>
    <?php
        $valueData = $all['aggregations']['characteristics']['char_' . $value->getId()]['ids'];
        $valueCount = $filter['aggregations']['characteristics']['char_' . $value->getId()]['doc_count'];
    ?>
    <?php if ($value->getWidget() == Characteristic::WIDGET_PERIOD): ?>

        <!-- < ?php if ($valueData['count'] > 1 && $doc_count == $valueCount) { ?> -->
        <?php if ($valueData['count'] > 1) { ?>
            <?= $this->render('range-filter', [
                'searchForm' => $value, //$searchForm,
                'items' => [$valueData['min'] , $valueData['max']],
                'form' => $form,
                'from' => [
                    'attribute' => '[' . $i . ']from',
                    'id' => 'v-' . $i . '-from',
                    'value' => $value->from,
                ],
                'to' => [
                    'attribute' => '[' . $i . ']to',
                    'id' => 'v-' . $i . '-to',
                    'value' => $value->to,
                ],
                'title' => Html::encode($value->getCharacteristicName()),
            ]) ?>
        <?php }; ?>

    <?php elseif ($value->getWidget() == Characteristic::WIDGET_CHECKBOX): ?>

        <?php if ((count($valueData['buckets']) > 1 && $doc_count == $valueCount) || ($value->isFilled())) { ?>
            <?= $this->render('checkbox-filter', [
                'searchForm' => $value, //$searchForm,
                'items' => FilterHelper::checkboxVariants($filter['aggregations']['characteristics']['char_' . $value->getId()]['ids']['buckets'], $all['aggregations']['characteristics']['char_' . $value->getId()]['ids']['buckets']),
                'form' => $form,
                'attribute' => '[' . $i . ']equal',
                'title' => Html::encode($value->getCharacteristicName()),
            ]) ?>
        <?php }; ?>

    <?php endif; ?>
<?php endforeach; ?>

<?php endif; ?>


<?php ActiveForm::end() ?>

