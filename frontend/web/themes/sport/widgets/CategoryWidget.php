<?php

namespace frontend\web\themes\sport\widgets;

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\repositories\read\ShopCategoryReadRepository;
use common\modules\shop\repositories\read\views\ShopCategoryView;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\widgets\Menu;

class CategoryWidget extends Widget
{
    /** @var ShopCategory|null */
    public $active;

    private $_categories;

    public function __construct(ShopCategoryReadRepository $categories, $config = [])
    {
        parent::__construct($config);
        $this->_categories = $categories;
    }

    public function run(): string
    {
        $menuArray = ArrayHelper::toArray($this->_categories->getTreeWithSubsOf($this->active), [
            ShopCategoryView::class => [
                'label' => function ($view) {
                    $indent = ($view->category->depth > 1 ? str_repeat('&nbsp;&nbsp;&nbsp;', $view->category->depth - 1) : '');
                    return $indent . $view->category->translation->name . ' <span>' . $view->count . '</span>';
                },
                'url' => function ($view) {
                    return ['/shop/catalog/category', 'id' => $view->category->id];
                },
            ],
        ]);
        return Menu::widget([
            'items' => $menuArray,
            'encodeLabels' => false,
            'options' => [
                'class' => 'category_links',
            ],
        ]);
    }
}
