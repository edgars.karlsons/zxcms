<?php
/**
 * Created by Error202
 * Date: 23.10.2017
 */

namespace app\themes\sport\widgets;

use yii\base\Widget;
use app\modules\blog\repositories\read\PostReadRepository;

class MenuLastPostWidget extends Widget
{
    private $repository;

    public function __construct(PostReadRepository $repository, $config = [])
    {
        parent::__construct($config);
        $this->repository = $repository;
    }

    public function run(): string
    {
        return $this->render('menu-last-posts', [
            'posts' => $this->repository->getLast(3)
        ]);
    }
}