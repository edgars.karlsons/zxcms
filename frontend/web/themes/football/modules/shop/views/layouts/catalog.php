<?php
//use app\modules\shop\widgets\FilterWidget;
use frontend\widgets\Alert;
use frontend\web\themes\sport\widgets\CategoryWidget;
use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\web\View
 * @var $content string
 */

$this->title = 'oldenburger Shop - Catalog'; //Yii::$app->settings->data['system']['meta_title'];
?>

<?php $this->beginContent('@frontend/web/themes/sport/layouts/main.php') ?>

<div class="row">
    <div class="col-sm-3">
        <aside class="left-box" role="complementary" aria-label="Боковое меню">
            <nav role="navigation" aria-label="Тематические категории">
                <?= CategoryWidget::widget([
                    'active' => $this->params['active_category'] ?? null
                ]) ?>
            </nav>

            <!--<div class="sidebar_widget sidebar_widget__filter">
                < ?= FilterWidget::widget([]) ?>
            </div> -->

        </aside>
    </div>
    <div class="main-content col-sm-9">
        <!-- BREADCRUMBS -->
        <div class="breadcrumb_wrap">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>

        <?= Alert::widget() ?>

        <?= $content ?>

    </div>
</div>

<?php $this->endContent() ?>


