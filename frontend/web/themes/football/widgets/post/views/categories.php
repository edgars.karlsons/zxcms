<?php
/**
 * @var $this \yii\web\View
 * @var $categories \core\entities\post\PostCategory[]
 */

use yii\helpers\Url;

?>

<div class="block-aside-item">
	<!-- Heading Component-->
	<article class="heading-component">
		<div class="heading-component-inner">
			<h5 class="heading-component-title"><?= Yii::t('post', 'Categories') ?>
			</h5>
		</div>
	</article>

	<!--Block Categories-->
	<div class="block-categories">
		<ul class="list-marked list-marked-categories">
			<?php foreach ($categories as $category): ?>
				<li><a href="<?= Url::to(['post/category', 'id' => $category->id, 'tid' => $category->type_id]) ?>"><?= $category->name ?></a><span class="list-marked-counter"><?= $category->getPostsCount() ?></span></li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
