<?php

use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $view string
 * @var $posts \core\entities\post\Post[]
 */

?>

<div class="block-aside-item">
	<!-- Heading Component-->
	<article class="heading-component">
		<div class="heading-component-inner">
			<h5 class="heading-component-title"><?= Yii::t('post', 'In The Spotlight') ?>
			</h5><a class="button button-xs button-gray-outline" href="<?= Url::to(['post/index', 'tid' => 2]) ?>"><?= Yii::t('post', 'News') ?></a>
		</div>
	</article>

<div class="list-post-classic">
	<?php foreach ($posts as $post): ?>

		<?= $this->render($view . '_item', [
			'post' => $post
		]) ?>

	<?php endforeach; ?>
</div>

</div>