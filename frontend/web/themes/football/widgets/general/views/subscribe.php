<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $model \core\forms\SubscribeForm
 */

$url = \yii\helpers\Url::to(['/site/subscribe']);
$successText = Yii::t('user', 'Your e-mail is subscribed!');
$js = <<<JS
 $('#subscribe-form').on('beforeSubmit', function(){
	 var data = $(this).serialize();
	 $.ajax({
	 url: '{$url}',
	 type: 'POST',
	 data: data,
	 success: function(res){
	 	var subContainer = $('#subscribe-container');
	 	subContainer.html('{$successText}');
	 	subContainer.css('padding', '20px');
	 },
	 error: function(){
	 	alert('Error: ' + res.message);
	 }
	 });
	 return false;
 });
JS;

$this->registerJs($js);
?>

<div class="block-aside-item">
	<!-- Heading Component-->
	<article class="heading-component">
		<div class="heading-component-inner">
			<h5 class="heading-component-title"><?= Yii::t('user', 'Join Our Newsletter') ?>
			</h5>
		</div>
	</article>

	<div id="subscribe-container">
		<!-- Mail Form Modern-->
		<?php $form = ActiveForm::begin([
			'id' => 'subscribe-form',
		]); ?>

			<div class="form-wrap">
				<?= $form->field($model, 'email', [
					'template' => '{input}{error}',
					'errorOptions' => [ 'class' => 'form-validation' ]
				])->textInput([
					'class' => 'form-input form-control-has-validation form-control-last-child',
					'placeholder' => Yii::t('user', 'Enter Your E-mail'),
				])->label(false) ?>
			</div>

			<div>
				<?= Html::submitButton(Yii::t('user', 'Subscribe'), [
					'class' => 'button button-block button-primary',
				]) ?>
			</div>

		<?php ActiveForm::end(); ?>
	</div>
</div>

