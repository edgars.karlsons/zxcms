<?php

use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\web\View
 * @var $content string
 */

?>
<?php $this->beginContent('@frontend/web/themes/sport/layouts/main.php') ?>


	<!-- Section Breadcrumbs-->
	<section class="section parallax-container breadcrumbs-wrap" data-parallax-img="/themes/sport/images/news.jpg">
		<div class="parallax-content breadcrumbs-custom context-dark">
			<div class="container">
				<h3 class="breadcrumbs-custom-title"><?= Yii::t('post', 'News') ?></h3>
				<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					'options' => ['class' => 'breadcrumbs-custom-path'],
					'encodeLabels' => false,
				]) ?>
			</div>
		</div>
	</section>

	<!-- Cintent-->
	<section class="section section-sm bg-gray-100">
		<div class="container">
			<div class="row row-30">
				<div class="col-lg-12">
					<?= \frontend\widgets\Alert::widget() ?>
					<?= $content ?>
				</div>
			</div>
		</div>
	</section>

<?php $this->endContent() ?>