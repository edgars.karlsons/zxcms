<?php

/* @var $this \yii\web\View */
/* @var $content string */


?>
<?php $this->beginContent('@frontend/views/layouts/main.php') ?>

<div class="row">
	<div id="content" class="col-sm-9">
		<?= $content ?>
	</div>
	<aside id="column-right" class="col-sm-3 hidden-xs">

	</aside>
</div>

<?php $this->endContent() ?>
