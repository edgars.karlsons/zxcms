<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class CookieAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-cookie';
    public $js = [
        'jquery.cookie.js',
    ];
}
