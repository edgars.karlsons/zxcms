<?php
namespace frontend\controllers\auth;

use core\services\auth\PasswordResetService;
use DomainException;
use Yii;
use yii\base\Exception;
use yii\web\BadRequestHttpException;
use core\forms\auth\PasswordResetRequestForm;
use core\forms\auth\ResetPasswordForm;
use yii\web\Controller;
use yii\web\Response;

class ResetController extends Controller
{
    public $layout = 'auth';

    private PasswordResetService $service;

    public function __construct($id, $module, PasswordResetService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * @return string|Response
     * @throws Exception
     */
    public function actionRequest(): string|Response
    {
        $form = new PasswordResetRequestForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->request($form);
                Yii::$app->session->setFlash('success', Yii::t('auth', 'Check your email for further instructions.'));
                return $this->goHome();
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('request', [
            'model' => $form,
        ]);
    }

    /**
     * @param $token
     * @return string|Response
     * @throws BadRequestHttpException|Exception
     */
    public function actionConfirm($token): string|Response
    {
        try {
            $this->service->validateToken($token);
        } catch (DomainException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $form = new ResetPasswordForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->reset($token, $form);
                Yii::$app->session->setFlash('success', Yii::t('auth', 'New password saved.'));
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
            return $this->goHome();
        }

        return $this->render('confirm', [
            'model' => $form,
        ]);
    }
}
