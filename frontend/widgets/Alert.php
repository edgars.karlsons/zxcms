<?php
namespace frontend\widgets;

use Yii;
use yii\base\Widget;

class Alert extends Widget
{
    public array $alertTypes = [
        'error'   => 'alert-danger',
        'danger'  => 'alert-danger',
        'success' => 'alert-success',
        'info'    => 'alert-info',
        'warning' => 'alert-warning'
    ];

    public function run()
    {
        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $flash) {
            if (!isset($this->alertTypes[$type])) {
                continue;
            }

	        foreach ((array) $flash as $message) {

		        $html = <<<HTML
<div class="alert {$this->alertTypes[$type]} alert-dismissible fade show" role="alert">
  {$message}
</div>
HTML;
		        echo $html;
	        }
            $session->removeFlash($type);
        }
    }
}
