<?php
/**
 * Created by Error202
 * Date: 24.01.2018
 */

namespace frontend\widgets\post;

use core\entities\post\Post;
use yii\base\Widget;
use yii\helpers\Url;

class PostWidget extends Widget
{
	const FILTER_LAST = 1;
	const FILTER_POPULAR = 2;

	public int $type;
	public int $count;
	public $view;
	public int $filter;

	public function __construct($config = [])
	{
		parent::__construct($config);
		$this->count = $this->count ?: 5;
	}

	public function run(): string
	{
		if ($this->filter == self::FILTER_POPULAR) {
			$posts = Post::find()->byType( $this->type )->popular()->limit($this->count)->all();
		}
		else {
			$posts = Post::find()->byType( $this->type )->last()->limit($this->count)->all();
		}
		return $this->render($this->view, [
			'posts' => $posts,
			'view' => $this->view,
			'url' => Url::canonical(),
		]);
	}
}