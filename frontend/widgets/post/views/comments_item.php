<?php

use core\entities\post\PostComment;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $comment PostComment
 */
?>

<!-- Comment-->
<div class="comment-classic">
	<svg version="1.1" x="0px" y="0px" width="6.888px" height="4.68px" viewbox="0 0 6.888 4.68" enable-background="new 0 0 6.888 4.68" xml:space="preserve">
                                          <path fill="#171617" d="M1.584,0h1.8L2.112,4.68H0L1.584,0z M5.112,0h1.776L5.64,4.68H3.528L5.112,0z"></path>
                                        </svg>
	<div class="comment-classic-header">
		<div class="comment-classic-header-main">
			<p class="comment-classic-title"><?= $comment->user->username ?></p>
			<time class="comment-classic-time" datetime="<?= date('Y', $comment->created_at) ?>">
				<?php
				$date = ($comment->created_at > time() - 60*60*24*365*50
					? Yii::$app->formatter->asRelativeTime($comment->created_at)
					: Yii::$app->formatter->asDatetime($comment->created_at));
				?>
				<?= $date ?>
			</time>
		</div>
	</div>
	<div class="comment-classic-body">
		<div class="comment-classic-text">
			<p>
				<?= StringHelper::truncateWords($comment->text, 20, '...') ?>
			</p>
		</div>
		<div class="comment-classic-post-title">
			<?= Html::a($comment->post->title, ['post/post', 'id' => $comment->post_id]) ?>
		</div>
	</div>
</div>
