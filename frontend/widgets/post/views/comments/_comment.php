<?php

/* @var $item CommentView */
/* @var $child integer */

use frontend\widgets\post\CommentView;

?>

<div data-id="<?= $item->comment->id ?>" class="post-comment <?= $child ? 'post-comment-child' : 'post-comment-parent' ?>" style="margin-left: <?= $child * 30 ?>px">
	<!--<div class="post-comment-aside">
		 <img class="img-circle" src="Blog%20post_files/user-4-69x69.jpg" alt="" width="69" height="69">
	</div>-->
	<div class="post-comment-main">
		<div class="post-comment-header">
			<a name="comment_<?= $item->comment->id ?>"></a>
			<h5 class="author-name"><?= $item->comment->user->username ?></h5>
			<time class="post-comment-time" datetime="<?= date('Y', $item->comment->created_at) ?>">
				<?php
				$date = ($item->comment->created_at > time() - 60*60*24*365*50
					? Yii::$app->formatter->asRelativeTime($item->comment->created_at)
					: Yii::$app->formatter->asDatetime($item->comment->created_at));
				?>
				<?= $date ?>
			</time>
		</div>
		<div class="post-comment-text">
			<p>
				<?php if ($item->comment->isActive()): ?>
					<?= Yii::$app->formatter->asNtext($item->comment->text) ?>
				<?php else: ?>
					<i>Comment is deleted.</i>
				<?php endif; ?>
			</p>
		</div>
		<div class="post-comment-footer">
			<!-- <div class="comment-like"><span class="icon fa fa-thumbs-o-up"></span><a href="#">32</a></div> -->
			<?php if (!Yii::$app->user->isGuest): ?>
				<div class="comment-reply"><span class="icon fa fa-comment"></span><a href="#"><?= Yii::t('post', 'Reply') ?></a></div>
			<?php endif; ?>
		</div>

		<div class="reply-block"></div>

	</div>
</div>



<?php foreach ($item->children as $children): ?>
	<?= $this->render('_comment', ['item' => $children, 'child' => $child + 1]) ?>
<?php endforeach; ?>
