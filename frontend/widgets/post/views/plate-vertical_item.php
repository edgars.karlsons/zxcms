<?php

use core\entities\post\Post;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $post Post
 * @var $url string
 */
?>

<div class="col-md-6">
	<!-- Post Future-->
	<article class="post-future">
		<a class="post-future-figure" href="#">
			<img src="<?= Html::encode($post->getThumbFileUrl('image', '368_287')) ?>" alt="<?= Html::encode($post->title) ?>" />
		</a>
		<div class="post-future-main">
			<h4 class="post-future-title"><?= Html::a(StringHelper::truncateWords(Html::encode($post->title), 6, '...'), '#') ?></h4>
			<div class="post-future-meta">
				<!-- Badge-->
				<div class="badge badge-secondary"><?= $post->category->name ?></div>
				<div class="post-future-time"><span class="icon fa fa-calendar"></span>
					<time datetime="<?= date('Y') ?>"><?= Yii::$app->formatter->asDate($post->published_at, 'php:d F, Y') ?></time>
				</div>
			</div>
			<hr>
			<div class="post-future-text">
				<p><?= StringHelper::truncateWords(Html::encode($post->description), 12, '...') ?></p>
			</div>
			<div class="post-future-footer group-flex group-flex-xs">
				<a class="button button-gray-outline" href="#"><?= Yii::t('post', 'Read more') ?></a>
				<div class="post-future-share">
					<div class="inline-toggle-parent">
						<div class="inline-toggle icon fa fa-share-alt" title="<?= Yii::t('post', 'Share') ?>"></div>
						<div class="inline-toggle-element">
							<ul class="list-inline">
								<li><span class="fa fa-share-alt"></span></li>
								<li><a target="_blank" class="icon fa fa-facebook" href="https://www.facebook.com/sharer.php?src=sp&u=<?= urlencode($url) ?>"></a></li>
								<li><a target="_blank" class="icon fa fa-odnoklassniki" href="https://connect.ok.ru/offer?url=<?= urlencode($url) ?>"></a></li>
								<li><a target="_blank" class="icon fa fa-google-plus" href="https://plus.google.com/share?url=<?= urlencode($url) ?>"></a></li>
								<li><a target="_blank" class="icon fa fa-vk" href="https://vk.com/share.php?url=<?= urlencode($url) ?>"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</div>