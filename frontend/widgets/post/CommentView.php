<?php

namespace frontend\widgets\post;

use core\entities\post\PostComment;

class CommentView
{
    public PostComment $comment;
    /**
     * @var self[]
     */
    public array $children;

    public function __construct(PostComment $comment, array $children)
    {
        $this->comment = $comment;
        $this->children = $children;
    }
}
