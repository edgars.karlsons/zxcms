<?php

use core\entities\post\Post;
use core\entities\post\PostType;
use frontend\widgets\post\CommentsWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $post Post */
/* @var $type PostType */

$this->title = $post->getSeoTitle();

$this->registerMetaTag(['name' =>'description', 'content' => $post->meta->description]);
$this->registerMetaTag(['name' =>'keywords', 'content' => $post->meta->keywords]);

$this->params['breadcrumbs'][] = ['label' => $type->plural, 'url' => ['post/index', 'tid' => $type->id]];
$this->params['breadcrumbs'][] = ['label' => $post->category->name, 'url' => ['post/category', 'id' => $post->category->id]];
$this->params['breadcrumbs'][] = $post->title;

$this->params['active_category'] = $post->category;

$tagLinks = [];
foreach ($post->tags as $tag) {
    $tagLinks[] = Html::a(Html::encode($tag->name), ['tag', 'slug' => $tag->slug]);
}
?>

<article>
    <h1><?= Html::encode($post->title) ?></h1>

    <p><span class="glyphicon glyphicon-calendar"></span> <?= Yii::$app->formatter->asDatetime($post->published_at); ?></p>

    <?php if ($post->image): ?>
        <p><img src="<?= Html::encode($post->getThumbFileUrl('image', 'origin')) ?>" alt="" class="img-responsive" /></p>
    <?php endif; ?>

    <?= Yii::$app->formatter->asHtml($post->content, [
        'Attr.AllowedRel' => array('nofollow'),
        'HTML.SafeObject' => true,
        'Output.FlashCompat' => true,
        'HTML.SafeIframe' => true,
        'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
    ]) ?>
</article>

<p>Tags: <?= implode(', ', $tagLinks) ?></p>

<?= CommentsWidget::widget([
    'post' => $post,
]) ?>
