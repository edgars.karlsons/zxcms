<?php

use core\entities\post\Post;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Post */

$url = Url::to(['post/post', 'id' =>$model->id]);
?>

<div class="post-item">
    <?php if ($model->image): ?>
        <div>
            <a href="<?= Html::encode($url) ?>">
                <img src="<?= Html::encode($model->getThumbFileUrl('image', 'list')) ?>" alt="" class="img-responsive" />
            </a>
        </div>
    <?php endif; ?>
    <div class="h2"><a href="<?= Html::encode($url) ?>"><?= Html::encode($model->title) ?></a></div>
    <p><?= Yii::$app->formatter->asNtext($model->description) ?></p>
</div>


